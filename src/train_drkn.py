"""
Program: deep_radial_kernel_machine
File: train_drkn.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tensorflow as tf
import sklearn.svm as svm
import numpy.random as rand
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import math
import sys
import sklearn.datasets as data
from sklearn.multiclass import OneVsRestClassifier
import cPickle as pickle
import scipy.sparse as spsp
import sklearn.preprocessing as prep
from deep_radial_kernel_network import *
from rbf_network_svm import *
from data_set import *
from read_uci_data import read_uci_data
from sqlite_helper import SQL_Helper
import argparse
import itertools
import os
import os.path as osp
import logging

# warning: nasty global var hack
resultsdb = None # SQL_Helper()
STORE_RESULTS = False
GLOBAL_DROP_PROB=0.0
GLOBAL_L1_COEFF=0.0
GLOBAL_L2_COEFF=0.0


def run(problem, data, clf=None, svc=1.0, num_epochs=500,
        batch_size=50, delta=0.1, epochs_per_eval=10, drop_prob=0.0,
        network_type=DeepRadialKernelNetwork):
    """
    This is the generic run function using a standard data format.
    if clf is None, a standard svm will be learnt
    """
    if clf is None:
        print 'creating svm ...'
        clf = OneVsRestClassifier(svm.SVC(C=svc,gamma='auto',verbose=True), n_jobs=3)
        clf.fit(data.train_data, data.train_targets)
    elif type(clf)==str:
        # load in from file
        clf = pickle.load(open(clf)) 
    # else clf is already a classifier

    # plot_density_map_2d_svm(clf, data, problem)
    svmresults = clf.predict(data.test_data)
    diff = np.abs(svmresults-data.test_targets)
    svmerror = len(diff[diff!=0])/float(len(svmresults))
    print 'svmerror = ', svmerror
    if STORE_RESULTS:
        added = resultsdb.add_svmresults_to_db(problem, clf, svmerror)
        print added

    num_dims = data.train_data.shape[1]
    num_classes = data.train_targets_onehot.shape[1]

    deep_svm = network_type(clf, delta, drop_prob, resultsdb, STORE_RESULTS, l1_coeff=GLOBAL_L1_COEFF, l2_coeff=GLOBAL_L2_COEFF)

    return deep_svm.fit(problem, data, num_epochs=num_epochs,
                        batch_size=batch_size, epochs_per_eval=epochs_per_eval)


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--store', dest='store',
                        help='store the results in the results db',
                        type=bool, default=True)
    parser.add_argument('--db', dest='db',
                        help='the database filename to use',
                        type=str, default='../drkn_models/results.db')
    parser.add_argument('--drop_prob', dest='drop_prob', nargs='+',
                        help='the probability of dropping an SV',
                        type=float, default=[0.0])
    parser.add_argument('--l1_coeff', dest='l1_coeff', nargs='+',
                        help='coefficient for l1 regularisation',
                        type=float, default=[0.0])
    parser.add_argument('--l2_coeff', dest='l2_coeff', nargs='+',
                        help='coefficient for l2 regularisation',
                        type=float, default=[0.0])
    parser.add_argument('--svms', dest='svms', nargs='+',
                        help='the list of support vector machine files for initialisation - must be pickled from an sklearn svm',
                        type=str)
    parser.add_argument('--train_data', dest='train_data', nargs='+',
                        help='the list of training data files - pickled form of DataSet',
                        type=str)
    parser.add_argument('--network_type', default='DeepRadialKernelNetwork',
                        help='the type of network to train. Options are: DeepRadialKernelNetwork; or RBFKernel',
                        type=str)
    args = parser.parse_args()
    resultsdb = SQL_Helper(args.db)
    STORE_RESULTS = args.store

    logging.basicConfig(filename='drkn_training.log')
    
    svms = args.svms
    data = args.train_data 

    if args.network_type=='DeepRadialKernelNetwork':
        network_type = DeepRadialKernelNetwork
    elif args.network_type=='RBFKernel':
        network_type = OneVsRestRBFN

    # need to match up the svms with the train data
    problems = []
    for datum in data:
        splitname = osp.split(datum)
        justfile = splitname[-1]
        index = justfile.index('_data.pkl')
        dataname = justfile[:index]

        svmname = next((x for x in svms if dataname+'_ovr.skl' in x), None)
        if svmname is None:
            continue
        problems += [(datum, svmname, dataname)]

    for problem, GLOBAL_DROP_PROB, GLOBAL_L1_COEFF, GLOBAL_L2_COEFF in itertools.product(problems, args.drop_prob, args.l1_coeff, args.l2_coeff):
        try:
            print 'running ', problem[0], GLOBAL_DROP_PROB, GLOBAL_L1_COEFF, GLOBAL_L2_COEFF
            data = pickle.load(open(problem[0], 'rb'))
            svm = pickle.load(open(problem[1], 'rb'))
            results = run(problem[2], data, svm, network_type=network_type)
            print problem[2] + ': best acc, best_epoch', results
        except Exception as e:
            logging.error(str(e))
            instance = str(problem) + str(GLOBAL_DROP_PROB) + str(GLOBAL_L1_COEFF) + str(GLOBAL_L2_COEFF)
            logging.error('Error with ' + instance)
            print 'Error with ', instance
        
    # results = run_whitewine()
    # print 'whitewine: best acc, best_epoch', results
    # results = run_redwine()
    # print 'redwine: best acc, best_epoch', results
    # results = run_covtype()
    # print 'covtype: best acc, best_epoch', results
    
