"""
Program: deep_radial_kernel_machine
File: random_utils.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import numpy.random as rand

def norms(points):
    numpoints = points.shape[1]
    return np.sqrt([np.sum(np.dot(points[:,i:i+1].T,points[:,i:i+1]))
                    for i in xrange(numpoints)])

def random_uniform_in_sphere(r, num_dims, numpoints):
    """
    Generate numpoints uniform random points inside a sphere of num_dims.
    This uses a method from [1] (according to this math overflowstackexchange 
    post:
    https://stats.stackexchange.com/questions/7977/how-to-generate-uniformly-distributed-points-on-the-surface-of-the-3-d-unit-sphe

    [1] Harman, R. & Lacko, V. On decompositional algorithms for uniform 
    sampling from nn-spheres and nn-balls Journal of Multivariate Analysis, 
    2010
    """
    # first generate more than we need
    array = rand.rand(num_dims+2, numpoints)

    radii = norms(array)
    for i in xrange(numpoints):
        array[:,i] /= radii[i]

    return array[:num_dims,:]*r
            

if __name__=='__main__':
    nums = random_uniform_in_sphere(8.0, 100, 1000)
    import pdb; pdb.set_trace()
