"""
Program: deep_radial_kernel_machine
File: data_set.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import numpy.random as rand
import sklearn.preprocessing as prep

class ZTransform(object):
    """
    This class performs a z-transform based data used to initialise class. 
    The z-transform independently transforms each dimension to have mean 0
    and variance 1.
    If there is a constant feature, it is deleted from the data.
    Data is not stored here, just the transform.
    """

    def __init__(self, data=None, means=None, stds=None):
        """
        data is an array of shape (num_samples, num_dimensions)
        means is an array or a number containing the mean of the transform
        stds is an array or a number containing the stds of the transform
        I'm doing a bit of a hack here to get function overloading.
        The class should be instantiated by either:
        z = ZTransform(data) # estimate means/stds from data
        or
        z = ZTransform(means, stds) # specify means/stds directly

        if means and stds are a single number, then they are applied to
        all features. Otherwise they should be of shape (1, num_dimensions)
        """
        if data is not None:
            num_samples, num_dimensions = data.shape
            self.means = np.nanmean(data, 0)
            self.means = self.means.reshape((1, num_dimensions))
            self.stds = np.nanstd(data, 0)
            self.stds = self.stds.reshape((1, num_dimensions))
        
            self.delete_features = [x for x in xrange(num_dimensions)
                                    if self.stds[0,x]==0.0]
            self.means = np.delete(self.means, self.delete_features, 1)
            self.stds = np.delete(self.stds, self.delete_features, 1)
        elif means is not None and stds is not None:
            self.means = means
            self.stds = stds
        else:
            raise ValueError('Should only construct ZTransform with either \n' +
                             'ZTransform(data) or ZTransform(means, stds)')

    def transform(self, data):
        data = np.delete(data, self.delete_features, 1)
        data = (data-self.means)/self.stds
        return data
        
        

class DataSet(object):

    def __init__(self, data, targets=None, validation_data=None,
                 validation_targets=None, test_data=None, test_targets=None,
                 normalise=True, ztransform=None, shuffle=True,
                 validation_ratio=0.2, test_ratio=0.2):
        """
        Each row is a sample, each column is a feature
        targets is a 1D array
        """
        data = data[np.isfinite(data).all(axis=1),:]
        data = data.astype(np.float32)
        if validation_data is not None:
            validation_data = validation_data[np.isfinite(validation_data).all(axis=1),:]
            validation_data = validation_data.astype(np.float32)
        if test_data is not None:
            test_data = test_data[np.isfinite(test_data).all(axis=1),:]
            test_data = test_data.astype(np.float32)
            
        self.data = data

        # we have no labels. In this case, make them all unknown (label 0)
        if targets is None:
            numrows = data.shape[0]
            targets = np.array([0]*numrows)
            
        self.targets = targets

        # normalise each feature to have mean 0 and std.dev 1
        self.test_data = test_data
        self.test_targets = test_targets
        self.validation_data = validation_data
        self.validation_targets = validation_targets
        if normalise:
            if ztransform is None:
                self.ztransform = ZTransform(self.data)
            else:
                self.ztransform = ztransform
            self.data = self.ztransform.transform(self.data)
            if self.validation_data is not None:
                self.validation_data = self.ztransform.transform(self.validation_data)
            if self.test_data is not None:
                self.test_data = self.ztransform.transform(self.test_data)
        else:
            self.ztransform = ZTransform(means=0.0, stds=1.0)

        # now extract out training and testing
        # shuffle
        if shuffle:
            indexes = np.arange(data.shape[0])
            rand.shuffle(indexes)
            sdata = self.data[indexes,:]
            stargets = self.targets[indexes]
        else:
            sdata = self.data
            stargets = self.targets

        # split into train, valid and test
        if validation_data is None and test_data is None:
            num_train = int(data.shape[0]*(1.0-validation_ratio-test_ratio))
            self.train_data = sdata[:num_train,:]
            self.train_targets = stargets[:num_train]
            num_valid = int(data.shape[0]*validation_ratio)
            cum_num = num_train+num_valid
            self.validation_data = sdata[num_train:cum_num,:]
            self.validation_targets = stargets[num_train:cum_num]
            self.test_data = sdata[cum_num:,:]
            self.test_targets = stargets[cum_num:]
        elif validation_data is None:
            train_ratio = 1.0-validation_ratio-test_ratio
            new_total = train_ratio+validation_ratio
            new_train = train_ratio/new_total
            new_valid = validation_ratio/new_total
            num_train = int(data.shape[0]*(1.0-new_valid))
            self.train_data = sdata[:num_train,:]
            self.train_targets = stargets[:num_train]
            self.validation_data = sdata[num_train:,:]
            self.validation_targets = stargets[num_train:]
        else:
            self.train_data = sdata
            self.train_targets = stargets
        self.num_train = self.train_data.shape[0]
        self.num_test = self.test_data.shape[0]

        oh = prep.OneHotEncoder()
        if self.num_train>0:
            targets = self.train_targets.reshape(-1,1)
        elif self.num_test>0:
            targets = self.test_targets.reshape(-1,1)
        oh.fit(targets)
        self.train_targets_onehot = oh.transform(targets).toarray().astype(np.float32)
        targets = self.validation_targets.reshape(-1,1)
        self.validation_targets_onehot = oh.transform(targets).toarray().astype(np.float32)
        targets = self.test_targets.reshape(-1,1)
        self.test_targets_onehot = oh.transform(targets).toarray().astype(np.float32)

    def num_training(self):
        return self.train_data.shape[0]

    def shuffle_train(self):
        indexes = np.shuffle(np.arange(self.train_data.shape[0]))
        self.train_data = self.train_data[indexes,:]
        self.train_targets = self.train_targets[indexes]
        
    def train_minibatches(self, batch_size):
        base_index = 0
        while base_index<self.num_train:
            data = self.train_data[base_index:base_index+batch_size,:]
            targets = self.train_targets_onehot[base_index:base_index+batch_size]
            base_index += batch_size
            yield data, targets

    def train_minibatches_nothot(self, batch_size):
        base_index = 0
        while base_index<self.num_train:
            data = self.train_data[base_index:base_index+batch_size,:]
            targets = self.train_targets[base_index:base_index+batch_size]
            base_index += batch_size
            yield data, targets

    def validation_minibatches(self, batch_size):
        num_validation = self.validation_data.shape[0]
        base_index = 0
        while base_index<num_validation:
            data = self.validation_data[base_index:base_index+batch_size,:]
            targets = self.validation_targets_onehot[base_index:base_index+batch_size]
            base_index += batch_size
            yield data, targets

    def validation_minibatches_nothot(self, batch_size):
        num_validation = self.validation_data.shape[0]
        base_index = 0
        while base_index<num_validation:
            data = self.validation_data[base_index:base_index+batch_size,:]
            targets = self.validation_targets[base_index:base_index+batch_size]
            base_index += batch_size
            yield data, targets

    def test_minibatches(self, batch_size):
        base_index = 0
        # while base_index+batch_size<self.num_test:
        while base_index<self.num_test:
            data = self.test_data[base_index:base_index+batch_size,:]
            targets = self.test_targets_onehot[base_index:base_index+batch_size]
            base_index += batch_size
            yield data, targets

    def test_minibatches_nothot(self, batch_size):
        base_index = 0
        while base_index<self.num_test:
            data = self.test_data[base_index:base_index+batch_size,:]
            targets = self.test_targets[base_index:base_index+batch_size]
            base_index += batch_size
            yield data, targets
            

if __name__=='__main__':
    import sklearn.datasets as data
    import cPickle as pickle
    iris = data.load_iris()
    mydata = DataSet(iris['data'], targets=iris['target'], normalise=False)
    import pdb; pdb.set_trace()
    pickle.dump(mydata, open('iris_data.pkl', 'wb'), protocol=-1)
