"""
Program: deep_radial_kernel_machine
File: plot_utils.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tensorflow as tf
import sklearn.svm as svm
import numpy.random as rand
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import math
import sys
import sklearn.datasets as data
from sklearn.multiclass import OneVsRestClassifier
from tensorflow.examples.tutorials.mnist import input_data
import cPickle as pickle
import scipy.sparse as spsp
import sklearn.preprocessing as prep
from rotate_utils import *
from sqlite_helper import SQL_Helper
from deep_kernel import DeepKernel


def plot_density_map_2d_svm(clf, data, problem, h=0.02):
    spiral_data = data.train_data
    x_min, x_max = spiral_data[:, 0].min() - 0.1, spiral_data[:, 0].max() + .1
    y_min, y_max = spiral_data[:, 1].min() - .1, spiral_data[:, 1].max() + .1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    test_data = np.c_[xx.ravel(), yy.ravel()].T
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.figure()
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)

    # plt.scatter(spiral_data[:200,0],xlin[:200,1], color='blue')
    # plt.scatter(xlin[200:400,0], xlin[200:400,1], color='red')   
    plt.savefig("svm_{:s}.pdf".format(problem))


def plot_density_map_2d(sess, X, y_, deep_svm, data, problem, epoch=0, batch_size=50, h=0.02):
    spiral_data = data.train_data
    x_min, x_max = spiral_data[:, 0].min() - 0.1, spiral_data[:, 0].max() + .1
    y_min, y_max = spiral_data[:, 1].min() - .1, spiral_data[:, 1].max() + .1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    test_data = np.c_[xx.ravel(), yy.ravel()].T

    out = np.ndarray((1, test_data.shape[1]))
    for i in xrange(test_data.shape[1]/batch_size):
        print 'evaluating batch ', i, ' out of ', test_data.shape[1]/batch_size
        bstart = i*batch_size
        bend = (i+1)*batch_size

        batch = test_data[:,bstart:bend]
        Z = sess.run(deep_svm.output, feed_dict={X: batch})
        out[0,bstart:bend] = Z

    out = out.reshape(xx.shape)
    plt.figure()
    plt.contourf(xx, yy, out, cmap=plt.cm.coolwarm, alpha=0.8)
    plt.savefig("deep_{:s}_{:03d}.pdf".format(problem,epoch))
