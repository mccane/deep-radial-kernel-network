"""
Program: deep_radial_kernel_machine
File: compact_kernel.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import tensorflow as tf
import numpy as np
import numpy.random as npr
import math

class CKNonTensorFlowParams(object):
    """
    for storing the parameters in an np array form
    """

class CompactKernel(object):
    """
    This class implements Q_3,1 from:
    Fornefett, Mike, Karl Rohr, and H. Siegfried Stiehl. "Radial basis functions with compact support for elastic registration of medical images." Image and vision computing 19.1 (2001): 87-96.

    which is a pretty good approximation to the Gaussian kernel but with
    compact support.

    The class structure is used just to provide derivatives etc.
    """

    def __init__(self, sigma):
        """
        sigma is the std.dev. of the Gaussian kernel we are approximating
        """
        self.sigma = sigma
        self.ntfp = CKNonTensorFlowParams()

        # this is for linear approximation of kernel
        # 0.94 sigma is the only inflection point and is the point of
        # maximum absolute gradient
        # therefore all other points will have maxima at the ends of
        # their respective intervals
        self.intervals = [0.0, 0.94*sigma, 3.76*sigma]
        self.devals = [abs(self.deval(r)) for r in self.intervals]
        self.delta = None

    def eval(self, r):
        e = (1-r/(3.76*self.sigma))**4 * (4*r/(3.76*self.sigma) + 1)
        return e

    def geval(self, r):
        e = math.exp(-r*r/(2.0*self.sigma**2))
        return e

    def deval(self, r):
        d = 1.06383*(1-0.265957*r/self.sigma)**4/self.sigma
        d -= 1.06383*(1-0.265957*r/self.sigma)**3 * (1+1.06383*r/self.sigma)/self.sigma
        return d

    def max_abs_d(self):
        """
        the maximum magnitude of the derivative
        """
        return abs(self.deval(self.min_r()))
        
    def min_r(self):
        return 0.94*self.sigma

    def approximate_to_delta_recurse(self, intervals, devals, delta):
        dmax = max(devals)
        # stopping condition - good approximation reached
        if dmax*(intervals[1]-intervals[0])<=delta:
            return intervals
        # else do each sub-interval
        mid = (intervals[0]+intervals[1])/2.0
        dmid = abs(self.deval(mid))
        i1 = [intervals[0]] + [mid]
        d1 = [devals[0]] + [dmid]
        i1 = self.approximate_to_delta_recurse(i1, d1, delta)

        i2 = [mid] + [intervals[1]]
        d2 = [dmid] + [devals[1]]
        i2 = self.approximate_to_delta_recurse(i2, d2, delta)

        return i1 + i2[1:]

    def approximate_to_delta(self, delta):
        # only do this if we have to
        if delta!=self.delta:
            self.delta = delta
            self.intervals = [0.0, 0.94*self.sigma, 3.76*self.sigma]
            self.devals = [abs(self.deval(r)) for r in self.intervals]
            i1 = self.approximate_to_delta_recurse(self.intervals, self.devals, delta)
            i2 = self.approximate_to_delta_recurse(self.intervals[1:], self.devals[1:], delta)
            self.intervals = i1 + i2[1:]
        self.devals = [abs(self.deval(r)) for r in self.intervals]
        return self.intervals
        
    def relu_approx(self, x, y):
        """
        This function uses a layer of relus to approximate the 
        compact kernel
        """
        w1 = [1]*len(x)

        b0 = [y[0]]
        w0 = [(y[1]-y[0])/(x[1]-x[0])]

        for i in xrange(1,len(x)-1):
            ysofar = self.approx_eval(x[i+1], w0, b0, w1)
            if ysofar<=y[i+1]:
                w0 += [(y[i+1]-ysofar)/(x[i+1]-x[i])]
            else:
                w0 += [-(y[i+1]-ysofar)/(x[i+1]-x[i])]
                w1[i] = -1
            b0 += [-w0[i]*x[i]]
        w0 += [0.0]
        b0 += [0.0]
        w1[len(x)-1] = 0.0
        return w0, b0, w1

    def approx_eval(self, x, w0, b0, w1):
        f = 0
        for i in xrange(len(w0)):
            f += w1[i]*max(0, w0[i]*x+b0[i])

        return f
        
    def make_tf_graph(self, xvar, delta):
        """
        xvar is the input variable. It should be 1D and should be positive
        """
        # first figure out the intervals needed if not already done
        self.approximate_to_delta(delta)
        # then build the approximation
        evals = [self.eval(x) for x in self.intervals]
        w0, b0, w1 = self.relu_approx(self.intervals, evals)

        # now build the tf variables to encapsulate the layer

        xshape = xvar.get_shape()
        w0 = np.array(w0, dtype=np.float32)
        dim0 = w0.shape[0]
        self.ntfp.w0 = w0.reshape((dim0, 1, 1))
        b0 = np.array(b0, dtype=np.float32)
        self.ntfp.b0 = b0.reshape((dim0, 1, 1))
        w1 = np.array(w1, dtype=np.float32)
        self.ntfp.w1 = w1.reshape((1, dim0))

        return self.make_tf_from_ntfp(xvar)


    def make_tf_from_ntfp(self, xvar):
        """
        This assumes the np arrays have already been set up.
        """
        self.w0 = tf.Variable(self.ntfp.w0, name="weights")
        self.b0 = tf.Variable(self.ntfp.b0, name="bias")
        self.w1 = tf.Variable(self.ntfp.w1, name="weights")
        xshape = xvar.get_shape()
        xvar = tf.expand_dims(xvar, 0)

        dim0 = self.ntfp.w0.shape[0]
        t0 = tf.nn.relu(tf.multiply(self.w0, xvar)+self.b0)
        t0 = tf.reshape(t0, (dim0,-1))
        t1 = tf.nn.relu(tf.matmul(self.w1, t0))
        self.output = tf.reshape(t1, (int(xshape[0]), -1))
        return self.output

    def make_ntfp_from_tf(self, sess):
        """
        Extract the numpy params from the graph
        """
        self.ntfp.w0 = sess.run(self.w0)
        self.ntfp.b0 = sess.run(self.b0)
        self.ntfp.w1 = sess.run(self.w1)

    def total_neurons(self):
        return len(self.intervals)


def test_compact_kernel():
    ck = CompactKernel(1.0)
    ck.approximate_to_delta(0.1)

    x = tf.placeholder(tf.float32, (1, None))
    kernel = ck.make_tf_graph(x, 0.01)

    data = npr.rand(1,100)*3.0
    evals = np.array([ck.eval(data[0,i]) for i in xrange(data.shape[1])])
    evals = np.reshape(evals, (1, data.shape[1]))
    gevals = np.array([ck.geval(data[0,i]) for i in xrange(data.shape[1])])
    gevals = np.reshape(gevals, (1, data.shape[1]))

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        approx = sess.run(kernel, feed_dict={x: data})
        
        diffs = evals-approx
        print 'diff = ', np.max(diffs)

if __name__=='__main__':
    test_compact_kernel()
