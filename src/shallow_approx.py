"""
Program: deep_radial_kernel_machine
File: shallow_approx.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import tensorflow as tf
import numpy as np
import numpy.random as npr
import math

class TooManyNeurons(Exception):
    def __init__(self, num):
        self.num = num
        
class ShallowNonTensorFlowParams(object):
    """
    For storing the parameters in np array form
    """

class ShallowApprox(object):
    """
    This class implements a shallow approximation (single hidden layer) of 
    a radially symmetric function to a given accuracy using ReLUs.
    This is based on a modified version of Lemma 19 from Eldan and Shamir.

    Eldan, R., Shamir, O., 2016. The power of depth for feedforward neural networks. JMLR: Workshop and Conference Proceedings 49, 134.
    """

    def __init__(self, f, df, delta, R, L):
        """
        f: R->R is the function to approximate
        df: R->R is the derivative of the function to approximate
        delta is how much an error we want.
        R is the maximum radius we are interested in
        L is the Lipschitz constant for f (the maximum gradient)
        """

        self.f = f
        self.df = df
        self.delta = delta
        self.R = R
        self.L = L
        self.ntfp = ShallowNonTensorFlowParams()
        self.approximate_to_delta()


    def approximate_to_delta(self):
        interval = self.delta/self.L
        self.intervals = np.linspace(0.0,self.R,int(self.R/interval))
        return self.intervals

    def approx_eval(self, x, w0, b0, w1):
        f = 0
        for i in xrange(len(w0)):
            f += w1[i]*max(0, w0[i]*x+b0[i])

        return f
        

    def relu_approx(self, x, y):
        """
        This function uses a layer of relus to approximate the 
        input function.
        x: the x-values 
        y: corresponding function values
        """
        w1 = [1]*len(x)

        b0 = [y[0]]
        w0 = [(y[1]-y[0])/(x[1]-x[0])]

        for i in xrange(1,len(x)-1):
            ysofar = self.approx_eval(x[i+1], w0, b0, w1)
            if ysofar<=y[i+1]:
                w0 += [(y[i+1]-ysofar)/(x[i+1]-x[i])]
            else:
                w0 += [-(y[i+1]-ysofar)/(x[i+1]-x[i])]
                w1[i] = -1
            b0 += [-w0[i]*x[i]]
        w0 += [0.0]
        b0 += [0.0]
        w1[len(x)-1] = 0.0
        return w0, b0, w1

    def make_tf_graph(self, xvar):
        
        """
        xvar is the input variable. It should be 1D and should be positive
        """
        # first figure out the intervals needed if not already done
        # then build the approximation
        evals = [self.f(x) for x in self.intervals]
        w0, b0, w1 = self.relu_approx(self.intervals, evals)

        # now build the tf variables to encapsulate the layer

        xshape = xvar.get_shape()
        w0 = np.array(w0, dtype=np.float32)
        dim0 = w0.shape[0]
        self.ntfp.w0 = w0.reshape((dim0, 1, 1))
        b0 = np.array(b0, dtype=np.float32)
        self.ntfp.b0 = b0.reshape((dim0, 1, 1))
        w1 = np.array(w1, dtype=np.float32)
        self.ntfp.w1 = w1.reshape((1, dim0))

        return self.make_tf_from_ntfp(xvar)

    def make_tf_from_ntfp(self, xvar):
        """
        This assumes the np arrays have already been set up.
        """
        self.w0 = tf.Variable(self.ntfp.w0, name="weights")
        self.b0 = tf.Variable(self.ntfp.b0, name="bias")
        self.w1 = tf.Variable(self.ntfp.w1, name="weights")
        xshape = xvar.get_shape()
        xvar = tf.expand_dims(xvar, 0)

        dim0 = self.ntfp.w0.shape[0]
        t0 = tf.nn.relu(tf.multiply(self.w0, xvar)+self.b0)
        t0 = tf.reshape(t0, (dim0,-1))
        t1 = tf.nn.relu(tf.matmul(self.w1, t0))
        self.output = tf.reshape(t1, (int(xshape[0]), -1))
        return self.output

    def make_ntfp_from_tf(self, sess):
        """
        Extract the numpy params from the graph
        """
        self.ntfp.w0 = sess.run(self.w0)
        self.ntfp.b0 = sess.run(self.b0)
        self.ntfp.w1 = sess.run(self.w1)

    def total_neurons(self):
        return len(self.intervals)


def test_shallow_approx():
    f = lambda x: x**2
    df = lambda x: 2*x
    shallow = ShallowApprox(f, df, 0.01, 4.0, 8.0)
    shallow.approximate_to_delta()

    x = tf.placeholder(tf.float32, (1, None))
    kernel = shallow.make_tf_graph(x)

    data = npr.rand(1,1000)*4.0
    evals = np.array([shallow.f(data[0,i]) for i in xrange(data.shape[1])])
    evals = np.reshape(evals, (1, data.shape[1]))

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        approx = sess.run(kernel, feed_dict={x: data})
        
        diffs = evals-approx
        print 'diff = ', np.max(diffs)
        print 'num_neurons = ', shallow.total_neurons()

if __name__=='__main__':
    test_shallow_approx()
