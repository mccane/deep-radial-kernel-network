"""
Program: deep_radial_kernel_machine
File: three_layer_approx.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import tensorflow as tf
import numpy as np
import numpy.random as npr
import math
from shallow_approx import *
import itertools
from random_utils import random_uniform_in_sphere
from deep_kernel import DeepKernel
from num_parameters import total_parameters

class TLANonTensorFlowParams(object):
    """
    For storing the parameters in np array form
    """

class ThreeLayerApprox(object):
    """
    This class implements a shallow approximation (single hidden layer) of 
    a radially symmetric function to a given accuracy using ReLUs.
    This is based on a modified version of Lemma 18 from Eldan and Shamir.

    Eldan, R., Shamir, O., 2016. The power of depth for feedforward neural networks. JMLR: Workshop and Conference Proceedings 49, 134.
    """

    def __init__(self, f, df, delta, R, L, num_dims, xvar):
        """
        f and df take as input sum(x**2)
        """
        self.num_dims = num_dims
        self.ntfp = TLANonTensorFlowParams()

        # first compute x_i**2
        xsquared = lambda x: x**2
        dxsquared = lambda x: 2.0*x
        self.xisquared = [0]*num_dims
        self.shallows = [0]*num_dims
        num_neurons = 0
        for i in xrange(num_dims):
            self.shallows[i] = ShallowApprox(xsquared, dxsquared, delta/num_dims, R, 2*R)
            if self.shallows[i].total_neurons()*num_dims>500000:
                raise TooManyNeurons(self.shallows[i].total_neurons()*num_dims)
            self.xisquared[i] = self.shallows[i].make_tf_graph(xvar[i:i+1,:])
            num_neurons += self.shallows[i].total_neurons()

        # self.sumxsquared = tf.minimum(sum(self.xisquared), R*R)
        self.sumxsquared = sum(self.xisquared)
        self.num_first_layer_neurons = num_neurons

        # now compute f(sum(x**2))
        self.f = ShallowApprox(f,df,delta,R*R,L)
        self.feval = self.f.make_tf_graph(self.sumxsquared)

    def total_neurons(self):
        return self.num_first_layer_neurons + self.f.total_neurons()

def gauss_approx2(r2, sigma):
    r = math.sqrt(r2)
    if r>3.76*sigma:
        return 0.0
    e = (1-r/(3.76*sigma))**4 * (4*r/(3.76*sigma) + 1)
    return e

def dgauss_approx2(r2, sigma):
    r = math.sqrt(r2)
    d = 1.06383*(1-0.265957*r/sigma)**4/sigma
    d -= 1.06383*(1-0.265957*r/sigma)**3 * (1+1.06383*r/sigma)/sigma
    return d

def gauss_approx(r, sigma):
    e = (1-r/(3.76*sigma))**4 * (4*r/(3.76*sigma) + 1)
    return e

def dgauss_approx(r, sigma):
    d = 1.06383*(1-0.265957*r/sigma)**4/sigma
    d -= 1.06383*(1-0.265957*r/sigma)**3 * (1+1.06383*r/sigma)/sigma
    return d


if __name__=='__main__':
    """
    Do some testing on three layer
    """

    import argparse

    parser = argparse.ArgumentParser(description='Test fn approx.')
    parser.add_argument('--num_dims', type=int, nargs='+', default=[10],
                        help='Number of dimensions of problem')
    parser.add_argument('--batch_size', type=int,
                        default=50,
                        help='batch size')
    parser.add_argument('--num_batches', type=int,
                        default=20,
                        help='Number of batches to test')
    parser.add_argument('--sigma', type=float, nargs='+', default=[1.0],
                        help='standard deviation of Gaussian')
    parser.add_argument('--delta', type=float, nargs='+', default=[0.1],
                        help='required accuracy (maximum error)')
    parser.add_argument('--outfile', type=str, default='three_layer_results.dat',
                        help='file to put results in')

    args = parser.parse_args()

    # ndimss = [2**i for i in xrange(1, int(math.log(args.num_dims)/math.log(2))+1)]
    ndimss = args.num_dims
    nbatches = args.num_batches
    batch_size = args.batch_size
    sigmas = args.sigma
    deltas = args.delta
    
    with open(args.outfile, 'a') as outfile:
        outfile.write('NumDims, Delta, Sigma, maxNormError, maxError, numNeurons\n')
    fstring = '{:d}, {:f}, {:f}, {:.2e}, {:.2e}, {:d}\n'
        
    for ndims,sigma,delta in itertools.product(ndimss,sigmas,deltas):
        X = tf.placeholder(tf.float32, shape=(ndims, None))
        f = lambda x: gauss_approx2(x, sigma)
        df = lambda x: dgauss_approx2(x, sigma)
        try :
            three_layer = ThreeLayerApprox(f, df, delta, 3.76*sigma, abs(df(0.94*sigma)), ndims, X)
    
            init = tf.global_variables_initializer()
            with tf.Session() as sess:
                max_norm_error = 0.0
                max_func_error = 0.0
                sess.run(init)

                for batch in xrange(nbatches):
                    data = random_uniform_in_sphere(3.0*sigma, ndims, batch_size)
                    
                    norm_approx = sess.run(three_layer.sumxsquared, feed_dict={X: data})
                    shallow_eval = sess.run(three_layer.feval, feed_dict={X: data})

                    norms = np.array([np.dot(data[:,i:i+1].T,data[:,i:i+1])[0,0] for i in xrange(data.shape[1])])
                    # norms = np.sqrt(norms)

                    norm_evals = np.array([gauss_approx2(norms[i], sigma) for i in xrange(norms.shape[0])])
                    norm_evals = np.reshape(norm_evals, (1, norms.shape[0]))
            
                    batch_max_norm_error = np.max(np.abs(norm_approx-norms))
                    max_norm_index = np.argmax(np.abs(norm_approx-norms))
                    batch_max_func_error = np.max(np.abs(shallow_eval-norm_evals))
                    max_index = np.argmax(np.abs(shallow_eval-norm_evals))
                    if batch_max_norm_error>max_norm_error:
                        max_norm_error = batch_max_norm_error
                    if batch_max_func_error>max_func_error:
                        max_func_error = batch_max_func_error

                print 'total params = ', total_parameters()
                print 'shallow = ', fstring.format(ndims, delta, sigma,
                                                   max_norm_error,
                                                   max_func_error,
                                                   three_layer.total_neurons())

                with open(args.outfile, 'a') as outfile:
                    outfile.write(fstring.format(ndims, delta, sigma,
                                                 max_norm_error,
                                                 max_func_error,
                                                 three_layer.total_neurons()))
        except TooManyNeurons as tmn:
            print 'shallow = ', fstring.format(ndims, delta, sigma,
                                               -1,
                                               -1,
                                               tmn.num)
            with open(args.outfile, 'a') as outfile:
                outfile.write(fstring.format(ndims, delta, sigma,
                                             -1,
                                             -1,
                                             tmn.num))
            
