import tensorflow as tf
import sklearn.svm as svm
import numpy.random as rand
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import math
import sys
import sklearn.datasets as data
from sklearn.multiclass import OneVsRestClassifier
from tensorflow.examples.tutorials.mnist import input_data
import cPickle as pickle
import scipy.sparse as spsp
import sklearn.preprocessing as prep
from rotate_utils import *

from deep_radial_kernel_network import *
num_dims = 5

class my_svm(object):
    def __init__(self, nsv=1):
        self._gamma = 1.0
        self.support_vectors_ = npr.random_sample((nsv, num_dims))
        self.dual_coef_ = np.ones((nsv,1))
        self.intercept_ = 0.0
        self.n_support_ = [nsv]

    def eval(self, x):
        diff = [x-self.support_vectors_[i:i+1,:].T for i in xrange(self.support_vectors_.shape[0])]
        print 'diffs = ', diff
        import pdb; pdb.set_trace()
        nsv = self.n_support_[0]
        dist = np.zeros((x.shape[1], nsv))
        for sv in xrange(nsv):
            for vec in xrange(x.shape[1]):
                dist[vec, sv] = np.dot(diff[sv][:,vec:vec+1].T, diff[sv][:,vec:vec+1])
        print 'dist = ', np.sqrt(dist)
        val = np.exp(-self._gamma*dist)
        val = np.dot(val,self.dual_coef_)+ self.intercept_
        return val
        
        

class my_clf(object):

    def __init__(self):
        self.estimators_ = [my_svm(nsv=2)]

    def eval(self, x):
        return self.estimators_[0].eval(x)


def test_deep_kernel():
    clf = my_clf()

    x = tf.placeholder(tf.float32, (num_dims, None))
    y_ = tf.placeholder(tf.float32, (None, 2))

    test = DeepRadialKernelNetwork(clf, 0.01, x, y_)

    samples = npr.random_sample((num_dims, 10))
    evals = clf.eval(samples)
    print 'svm evals = ', evals

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        e2 = sess.run(test.kernel_eval.kernel, feed_dict={x: samples})
        folds = sess.run(test.kernel_eval.fold, feed_dict={x: samples})
        print 'fold = ', folds
        print 'deep evals = ', e2[0,:]+e2[1,:]
        import pdb; pdb.set_trace()
        print 'done'

def test_deep_kernel2():
    clf = my_clf()
    clf = OneVsRestClassifier(clf)

    x = tf.placeholder(tf.float32, (num_dims, None))
    y_ = tf.placeholder(tf.float32, (None, 2))

    import pdb; pdb.set_trace()
    test = DeepRadialKernelNetwork(clf, 0.01, x, y_)

    samples = npr.random_sample((num_dims, 10))
    evals = clf.eval(samples)
    print 'svm evals = ', evals

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        e2 = sess.run(test.kernel_eval.kernel, feed_dict={x: samples})
        actual_kernel = sess.run(test.kernel_eval.actual_kernel, feed_dict={x: samples})
        print 'deep evals = ', e2[0,:]+e2[1,:]
        print 'actual kernel-e2 = ', actual_kernel-e2
        import pdb; pdb.set_trace()
        print 'done'


if __name__=='__main__':
    test_deep_kernel2()
    
