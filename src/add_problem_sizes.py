"""
Program: deep_radial_kernel_machine
File: add_problem_sizes.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This program is an afterthought really and shouldn't be needed. But is used to
backfill the database with the number of training examples in each problem. 
That allows us to check the results depending on the data size.
"""
import cPickle as pickle
from data_set import *
from sqlite_helper import SQL_Helper
import argparse
import os.path as osp

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--db', dest='db',
                        help='the database filename to use',
                        type=str, default='../drkn_models/results.db')
    parser.add_argument('--train_data', dest='train_data', nargs='+',
                        help='the list of training data files - pickled form of DataSet',
                        type=str)

    args = parser.parse_args()
    resultsdb = SQL_Helper(args.db)

    data = args.train_data 

    for datum in data:
        splitname = osp.split(datum)
        justfile = splitname[-1]
        index = justfile.index('_data.pkl')
        dataname = justfile[:index]

        data = pickle.load(open(datum, 'rb'))

        resultsdb.add_problem_size(dataname, data.num_training())
