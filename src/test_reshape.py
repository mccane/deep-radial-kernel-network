import numpy as np
import tensorflow as tf

tensor = np.arange(24)
t = np.reshape(tensor, [2, 3, 4])
print t.shape

print t

tensor = tf.Variable(t)

reshape = tf.reshape(tensor, [2, 12])

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    array = sess.run(reshape, feed_dict={})
    print array
    print array.shape
