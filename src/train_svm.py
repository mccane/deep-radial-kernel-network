from tensorflow.examples.tutorials.mnist import input_data
from sklearn import svm
import cPickle as pickle
from sklearn.multiclass import OneVsRestClassifier
import sklearn.datasets as skdata
import numpy as np
import numpy.random as rand
from read_uci_data import read_uci_data
import sys
import scipy.stats
import sklearn.grid_search as skg
import argparse
from random_svc import BaseRandomSVC, RandomSVC
import os.path as osp
import os
from data_set import *
import traceback

parser = argparse.ArgumentParser()
parser.add_argument("--problems", dest='problems',
                    help="the files containing problem data", type=str,
                    nargs='+')
parser.add_argument("--iters", dest='iters',
                    help='the number of random parameter choices',
                    type=int, default=20)
parser.add_argument('--njobs', dest='njobs', type=int, default=3,
                    help='the number of concurrent processes to run')
parser.add_argument('--gamma_scale', dest='gscale', type=float, default=0.1,
                    help='the scale of exponential distribution of gamma')
parser.add_argument('--C_scale', dest='cscale', type=float, default=100,
                    help='the scale of exponential distribution of C')
args = parser.parse_args()

problems = args.problems
niters = args.iters
njobs = args.njobs
gscale = args.gscale
cscale = args.cscale

for problem in problems:

    try:
        print 'training', problem

        head_tail = osp.split(problem)
        tail = head_tail[1]
        index = tail.find('_data.pkl')
        problem_name = tail[:index]

        data = pickle.load(open(problem, 'rb'))

        clf = OneVsRestClassifier(svm.SVC())
        # clf = OneVsRestClassifier(BaseRandomSVC(int(data.train_targets.shape[0]*0.1+1)))
        params = {'estimator__C': scipy.stats.expon(scale=cscale), 'estimator__gamma': scipy.stats.expon(scale=gscale)}

        train_indexes = range(data.train_data.shape[0])
        cv_indexes = range(data.train_data.shape[0],
                           data.train_data.shape[0]+data.validation_data.shape[0])
        train_data = np.concatenate((data.train_data, data.validation_data))
        random_search = skg.RandomizedSearchCV(clf, params, n_iter=niters, n_jobs=njobs, verbose=5, cv=[[train_indexes, cv_indexes]])

        train_targets = np.concatenate((data.train_targets, data.validation_targets))
        random_search.fit(train_data, train_targets)

        clf = random_search.best_estimator_

        print 'best_params = ', random_search.best_params_, 'best_score = ', random_search.best_score_
        print 'testing'
        res = clf.predict(data.test_data)

        print 'computing error'

        diff = np.abs(res-data.test_targets)
        error = len(diff[diff!=0])/float(len(res))

        print 'error =', error

        print 'dumping'
        pickle.dump(clf, open("../svms/svm_"+problem_name+"_ovr.skl",'wb'), protocol=-1) 

    except Exception as e:
        print 'Exception: ', e
        print traceback.print_exc()
        import pdb; pdb.set_trace()
