"""
Program: deep_radial_kernel_machine
File: read_uci_data.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import pandas
import numpy as np
from os.path import expanduser

def read_uci_data(fname, sep=' ', uci_dir=None, ignore_cols=[], na_values=None):
    if uci_dir is None:
        #home = expanduser('~')
        uci_dir = '../data/'

    fname = uci_dir+fname
    pb = pandas.read_table(fname, sep=sep, skipinitialspace=True, na_values=na_values)

    keys = list(pb.keys())
    class_index = keys.index('class')
    pb2 = np.array(pb)

    # assume the class either comes first or last
    targets = pb2[:,class_index]
    if class_index>0:
        data = pb2[:,:class_index]
    else:
        data = pb2[:,class_index+1:]

    for i in xrange(len(ignore_cols)):
        if ignore_cols[i]>class_index:
            ignore_cols[i] -= 1
    data = np.delete(data, ignore_cols, 1)
    
    return data, targets


