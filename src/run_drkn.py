"""
Program: deep_radial_kernel_machine
File: run_drkn.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tensorflow as tf
import sklearn.svm as svm
import numpy.random as rand
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import math
import sys
import sklearn.datasets as data
from sklearn.multiclass import OneVsRestClassifier
import cPickle as pickle
import scipy.sparse as spsp
import sklearn.preprocessing as prep
from deep_radial_kernel_network import *
from data_set import DataSet
from read_uci_data import read_uci_data
from sqlite_helper import SQL_Helper
import argparse

if __name__=='__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--data', type=str,
                        help='the data set to load')
    parser.add_argument('--svm', type=str,
                        help='the svm model to load')
    parser.add_argument('--deep_model', type=str, nargs='+',
                        help='the saved deep svm model')

    args = parser.parse_args()

    train_data, train_targets = pickle.load(open(args.data, 'rb'))

    for model in args.deep_model:
        deep_model = pickle.load(open(model))

        predicted = deep_model.predict(train_data)

        print model
        print 'predicted = ', predicted

    print 'actual = ', train_targets
