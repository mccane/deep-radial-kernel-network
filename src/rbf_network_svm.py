import tensorflow as tf
import sklearn.svm as svm
import numpy.random as rand
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import math
import sys
import sklearn.datasets as data
from sklearn.multiclass import OneVsRestClassifier
from tensorflow.examples.tutorials.mnist import input_data
import cPickle as pickle
import scipy.sparse as spsp
import sklearn.preprocessing as prep
from rotate_utils import *


class RBFKernel(object):

    def eval_actual_kernel(self, diffs):
        shape = tf.shape(diffs)
        new_shape = shape[1]*shape[2]
        numrows = shape[0]
        newdiffs = tf.reshape(diffs, [numrows, new_shape])
        matmul = tf.matmul(tf.transpose(newdiffs),
                           tf.matmul(self.cov_inv, newdiffs))
        self.shape = shape
        self.matmul = matmul
        self.diag = tf.diag_part(matmul)
        self.reshape = tf.reshape(tf.diag_part(matmul), [shape[1], shape[2]])
        matmul = tf.reshape(tf.diag_part(matmul), [shape[1], shape[2]])
        
        self.exp = tf.exp(-matmul)
        return self.exp

    def __init__(self, support_vectors, gamma, X):
        """
        This is a shallow version - basically an RBF network initialised 
        with a SVM.
        gamma is the inverse of the Gaussian variance function
        X is the placeholder input

        """
        # this creates the appropriate intervals etc
        # libsvm apparently uses gamma = 1/(2 sigma^2)
        self.gamma = tf.Variable(gamma)
        self.sigma = 1.0/math.sqrt(2.0*gamma)
        # the variable here is the sqrt of the inverse covariance matrix
        # this saves getting tf to propagate the tricky derivative
        # should be more efficient (but I didn't check)
        self.sqrt_cov_inv = np.eye(support_vectors.shape[1], dtype=np.float32)/self.sigma
        self.sqrt_cov_inv = tf.Variable(self.sqrt_cov_inv)
        self.cov_inv = tf.matmul(self.sqrt_cov_inv, tf.transpose(self.sqrt_cov_inv))
        # first just do a single SVM

        self.centres = tf.Variable(support_vectors.T.astype(np.float32))
        self.expanded_centres = tf.expand_dims(self.centres, 2)
        expanded_inputs = tf.expand_dims(X, 1)
        self.diffs = tf.subtract(expanded_inputs, self.expanded_centres)

        self.actual_kernel = self.eval_actual_kernel(self.diffs)


class OneVsRestRBFN(object):
    
    def __init__(self, clf, delta, drop_prob=0.5, resultsdb=None,
                 STORE_RESULTS=False, l1_coeff=0.005, l2_coeff=0.001):
    # def __init__(self, clf, X, y_, drop_prob=0.5):

        self.clf = clf
        self.drop_prob = drop_prob
        self.delta = delta
        self.l1_coeff = l1_coeff
        self.l2_coeff = l2_coeff
        self.resultsdb = resultsdb
        if not STORE_RESULTS:
            self.resultsdb = None
        self.gamma = self.clf.estimators_[0]._gamma

        self.outputs = [] # all the outputs from each of the individual classifiers
        all_support_vectors = np.vstack([x.support_vectors_ for x in clf.estimators_])
        n_support = [sum(x.n_support_) for x in clf.estimators_]
        n_support = np.array(n_support, dtype=np.int32)
        all_coefs = np.hstack([x.dual_coef_ for x in clf.estimators_]).astype(np.float32)
        all_intercepts = np.hstack([x.intercept_ for x in clf.estimators_]).astype(np.float32)
        
        all_coefs = tf.Variable(all_coefs.T, name="weights")
        self.all_intercepts = tf.Variable(all_intercepts.reshape((len(clf.estimators_),1)))
        self.num_dims = clf.estimators_[0].support_vectors_.shape[1]
        self.num_classes = len(clf.estimators_)
        self.X = tf.placeholder(tf.float32, (self.num_dims, None))
        self.y_ = tf.placeholder(tf.float32, (None, self.num_classes))

        self.kernel_eval = RBFKernel(all_support_vectors, self.gamma, self.X)
        # raise RuntimeError()

        self.all_coefs = all_coefs
        vector_strength = tf.multiply(all_coefs, self.kernel_eval.actual_kernel)
        self.vector_strength = vector_strength

        segments = []
        for s in xrange(len(n_support)):
            segments += [s]*n_support[s]

        self.segment_sum = tf.segment_sum(vector_strength, tf.constant(segments))

        self.outputs = 0.5+(0.5*tf.tanh(self.segment_sum+self.all_intercepts))
        # need to augment for a single classifier
        if len(self.clf.estimators_)==1:
            out2 = 1.0-self.outputs
            self.outputs = tf.concat(0, [out2, self.outputs])
            
        self.output = tf.argmax(self.outputs, 0)

        self.softmax = tf.nn.softmax(tf.transpose(self.outputs))

        self.cross_entropy = tf.reduce_mean(-tf.reduce_sum(self.y_ * tf.log(self.softmax), reduction_indices=[1]))
        weights = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='weights')
        self.l1_term = tf.add_n([tf.reduce_sum(tf.abs(wt)) for wt in weights])
        self.l2_term = tf.add_n([tf.reduce_sum(tf.square(wt)) for wt in weights])
        self.loss = (self.cross_entropy+self.l1_coeff*self.l1_term+
                     self.l2_coeff*self.l2_term)

        print 'building optimiser (calculating gradients) ...'
        self.train = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy)
        print 'done that'
        
        self.correct = tf.equal(self.output, tf.argmax(self.y_,1))
        self.accuracy = tf.reduce_mean(tf.cast(self.correct, "float"))


    # need to refactor so the fit, evaluate_test etc is in a parent
    # class, and only the architecture specific stuff is in the sub-classes
    def fit(self, problem, data, num_epochs=100, batch_size=50, 
            epochs_per_eval=10, early_stopping_epochs=100):
        """
        The function that learns the model.
        problem is a string
        data must be a DataSet object

        The best model is saved to disk if it is best at the
        evaluation point (every epochs_per_eval)
        """

        # need to save this so we can transform correctly when predicting
        # or when loading in a saved model
        self.ztransform = data.ztransform
        
        init = tf.global_variables_initializer()

        with tf.Session() as sess:
            print 'initializing ...'
            sess.run(init)
            print 'evaluating initial accuracy ...'
            best_validation_acc = self.evaluate_validation(sess, data, batch_size=batch_size)
            train_acc = self.evaluate_train(sess, data, batch_size=batch_size)
            best_test_acc = self.evaluate_test(sess, data, batch_size=batch_size)
            best_epoch = -1
            print 'initial acc = ', best_test_acc, best_validation_acc, train_acc

            if self.resultsdb is not None:
                added = self.resultsdb.add_rbfresults_to_db(
                    problem, self.clf, best_epoch,
                    1.0-best_test_acc, 1.0-best_validation_acc, 1.0-train_acc,
                    self.drop_prob,
                    self.l1_coeff, self.l2_coeff)
                print added

            # now do training
            for epoch in xrange(num_epochs):
                print 'training epoch ', epoch, ' out of ', num_epochs

                for d, t in data.train_minibatches(batch_size):
                    sess.run(self.train, feed_dict={self.X: d.T, self.y_: t})

                # evaluate on test data
                if (epoch+1)%epochs_per_eval==0:
                    print 'testing ...', 
                    acc = self.evaluate_validation(sess, data, batch_size=batch_size)
                    train_acc = self.evaluate_train(sess, data,
                                                    batch_size=batch_size)
                    test_acc = self.evaluate_test(sess, data, batch_size=batch_size)
                    print 'test/valid/train acc = ', test_acc, acc, train_acc
                    if self.resultsdb is not None:
                        added = self.resultsdb.add_rbfresults_to_db(
                            problem, self.clf, epoch,
                            1.0-test_acc, 1.0-acc, 1.0-train_acc, self.drop_prob,
                            self.l1_coeff, self.l2_coeff)
                        print added
                    if (acc>best_validation_acc):
                        best_validation_acc = acc
                        best_epoch = epoch
                        best_test_acc = test_acc
                    if epoch-best_epoch>early_stopping_epochs:
                        break

        return best_test_acc, best_validation_acc, best_epoch


    def predict(self, data, batch_size=50):
        """
        predict the class based on the fitted model.
        """
        # self.make_tf_from_ntfp()
        # init = tf.global_variables_initializer()

        if not isinstance(data, DataSet):
            data = DataSet(data, ztransform=self.ztransform,
                           shuffle=False, test_ratio=1.0)
            
        with tf.Session() as sess:
            print 'initializing ...'
            sess.run(init)
            outputs = np.zeros((data.num_test,))
            index = 0
            for d, t in data.test_minibatches(batch_size):
                batch_outputs = sess.run(self.output, feed_dict={self.X: d.T})
                size = batch_outputs.shape[0]
                outputs[index*batch_size:index*batch_size+size] = batch_outputs[:] 
                index += 1
                
        return outputs


    def evaluate_train(self, sess, data, batch_size=50, max_loops=1e10):
        accuracy = 0.0
        num_done = 0
        for d, t in data.train_minibatches(batch_size):
            accuracy += sess.run(self.accuracy,
                                 feed_dict={self.X: d.T, self.y_: t})
            num_done += 1
            if num_done>max_loops:
                break

        return accuracy/num_done


    def evaluate_test(self, sess, data, batch_size=50):
        accuracy = 0.0
        num_done = 0
        # cov_inv = sess.run(self.kernel_eval.cov_inv,
                           # feed_dict={})
        # print 'cov inv = ', cov_inv
        for d, t in data.test_minibatches(batch_size):
            accuracy += sess.run(self.accuracy,
                                 feed_dict={self.X: d.T, self.y_: t})
            num_done += 1

        return accuracy/num_done

    def evaluate_validation(self, sess, data, batch_size=50):
        accuracy = 0.0
        num_done = 0
        for d, t in data.validation_minibatches(batch_size):
            accuracy += sess.run(self.accuracy,
                                 feed_dict={self.X: d.T, self.y_: t})

            num_done += 1

        return accuracy/num_done
