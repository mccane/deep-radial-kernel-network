"""
Program: deep_radial_kernel_machine
File: deep_radial_kernel_network.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tensorflow as tf
import sklearn.svm as svm
import numpy.random as rand
import numpy as np
import numpy.random as npr
import math
import sys
import sklearn.datasets as data
from sklearn.multiclass import OneVsRestClassifier
import cPickle as pickle
import sklearn.preprocessing as prep
from rotate_utils import *
from sqlite_helper import SQL_Helper
from deep_kernel import DeepKernel
from data_set import DataSet


class OVRNonTensorFlowParams(object):
    """
    This class stores all the parameters of the network in numpy arrays
    etc for pickling/unpickling.
    It doesn't actually contain anything explicitly and is constructed
    on the fly in the OneVsRestDeep class.
    """


class DeepRadialKernelNetwork(object):
    """
    This is the DRKN classifier taking a one vs rest Gaussian SVM and 
    approximating it with a Deep RELU network.

    This class broadly follows the sklearn API with fit and predict methods
    being the important "public" methods.
    """

    def __init__(self, clf, delta, drop_prob=0.5, resultsdb=None,
                 STORE_RESULTS=False, l1_coeff=0.005, l2_coeff=0.001):

        self.ntfp = OVRNonTensorFlowParams()

        self.ntfp.clf = clf
        self.ntfp.drop_prob = drop_prob
        self.ntfp.delta = delta
        self.ntfp.l1_coeff = l1_coeff
        self.ntfp.l2_coeff = l2_coeff
        self.resultsdb = resultsdb
        if not STORE_RESULTS:
            self.resultsdb = None

        gamma = self.ntfp.clf.estimators_[0]._gamma
        self.ntfp.gamma = gamma
        all_support_vectors = np.vstack([x.support_vectors_ for x in clf.estimators_])
        self.ntfp.all_support_vectors = all_support_vectors.T.astype(np.float32)

        n_support = [sum(x.n_support_) for x in clf.estimators_]
        self.ntfp.n_support = np.array(n_support, dtype=np.int32)
        all_coefs = np.hstack([x.dual_coef_ for x in clf.estimators_]).astype(np.float32)
        all_intercepts = np.hstack([x.intercept_ for x in clf.estimators_]).astype(np.float32)
        
        self.ntfp.all_coefs = all_coefs.T
        self.ntfp.all_intercepts = all_intercepts
        self.ntfp.num_estimators = len(clf.estimators_)
        self.ntfp.num_classes = len(clf.estimators_)
        if self.ntfp.num_classes==1:
            self.ntfp.num_classes=2
        self.ntfp.num_dims = clf.estimators_[0].support_vectors_.shape[1]

        self.X = tf.placeholder(tf.float32, (self.ntfp.num_dims, None))
        self.y_ = tf.placeholder(tf.float32, (None, self.ntfp.num_classes))
        self.all_intercepts = tf.Variable(self.ntfp.all_intercepts.reshape((self.ntfp.num_estimators,1)))

        self.all_support_vectors = tf.Variable(self.ntfp.all_support_vectors)
        self.kernel_eval = DeepKernel(self.all_support_vectors, 
                                      self.ntfp.gamma, 
                                      self.ntfp.delta, self.X, 
                                      self.ntfp.drop_prob)
        self.all_coefs = tf.Variable(self.ntfp.all_coefs, name="weights")
        self.vector_strength = tf.multiply(self.all_coefs, self.kernel_eval.kernel)

        segments = []
        for s in xrange(len(self.ntfp.n_support)):
            segments += [s]*self.ntfp.n_support[s]

        self.segment_sum = tf.segment_sum(self.vector_strength, tf.constant(segments))

        self.outputs = 0.5+(0.5*tf.tanh(self.segment_sum+self.all_intercepts))
        # need to augment for a single classifier
        if len(self.ntfp.clf.estimators_)==1:
            out2 = 1.0-self.outputs
            self.outputs = tf.concat(axis=0, values=[out2, self.outputs])
            
        self.output = tf.argmax(self.outputs, 0)

        self.softmax = tf.nn.softmax(tf.transpose(self.outputs))

        self.cross_entropy = tf.reduce_mean(-tf.reduce_sum(self.y_ * tf.log(self.softmax), axis=[1]))

        weights = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='weights')
        self.l1_term = tf.add_n([tf.reduce_sum(tf.abs(wt)) for wt in weights])
        self.l2_term = tf.add_n([tf.reduce_sum(tf.square(wt)) for wt in weights])
        self.loss = (self.cross_entropy+self.ntfp.l1_coeff*self.l1_term+
                     self.ntfp.l2_coeff*self.l2_term)

        print 'building optimiser (calculating gradients) ...'
        self.train = tf.train.AdamOptimizer(1e-4).minimize(self.loss)
        print 'done that'
        
        self.correct = tf.equal(self.output, tf.argmax(self.y_,1))
        self.accuracy = tf.reduce_mean(tf.cast(self.correct, "float"))


    def make_tf_from_ntfp(self):
        """
        All the tensorflow stuff is initialised here so that the
        graph can be rebuilt given a new set of parameters.
        This allows saving and loading a model using pickle.
        """
        self.X = tf.placeholder(tf.float32, (self.ntfp.num_dims, None))
        self.y_ = tf.placeholder(tf.float32, (None, self.ntfp.num_classes))
        self.all_intercepts = tf.Variable(self.ntfp.all_intercepts.reshape((self.ntfp.num_estimators,1)))

        self.all_support_vectors = tf.Variable(self.ntfp.all_support_vectors)
        self.kernel_eval.make_tf_from_ntfp(
            self.all_support_vectors, self.X)
        self.all_coefs = tf.Variable(self.ntfp.all_coefs, name="weights")
        self.vector_strength = tf.multiply(self.all_coefs, self.kernel_eval.kernel)

        segments = []
        for s in xrange(len(self.ntfp.n_support)):
            segments += [s]*self.ntfp.n_support[s]

        self.segment_sum = tf.segment_sum(self.vector_strength, tf.constant(segments))

        self.outputs = 0.5+(0.5*tf.tanh(self.segment_sum+self.all_intercepts))
        # need to augment for a single classifier
        if len(self.ntfp.clf.estimators_)==1:
            out2 = 1.0-self.outputs
            self.outputs = tf.concat(axis=0, values=[out2, self.outputs])
            
        self.output = tf.argmax(self.outputs, 0)

        self.softmax = tf.nn.softmax(tf.transpose(self.outputs))

        self.cross_entropy = tf.reduce_mean(-tf.reduce_sum(self.y_ * tf.log(self.softmax), axis=[1]))

        weights = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='weights')
        self.l1_term = tf.add_n([tf.reduce_sum(tf.abs(wt)) for wt in weights])
        self.l2_term = tf.add_n([tf.reduce_sum(tf.square(wt)) for wt in weights])
        self.loss = (self.cross_entropy+self.ntfp.l1_coeff*self.l1_term+
                     self.ntfp.l2_coeff*self.l2_term)

        print 'building optimiser (calculating gradients) ...'
        self.train = tf.train.AdamOptimizer(1e-4).minimize(self.loss)

        print 'building optimiser (calculating gradients) ...'
        self.train = tf.train.AdamOptimizer(1e-4).minimize(self.cross_entropy)
        print 'done that'
        
        self.correct = tf.equal(self.output, tf.argmax(self.y_,1))
        self.accuracy = tf.reduce_mean(tf.cast(self.correct, "float"))

        
    def make_ntfp_from_tf(self, sess):
        self.ntfp.all_support_vectors = sess.run(self.all_support_vectors)
        self.ntfp.all_intercepts = sess.run(self.all_intercepts)
        self.ntfp.all_coefs = sess.run(self.all_coefs)
        self.kernel_eval.make_ntfp_from_tf(sess)


    def __getstate__(self):
        """
        Note: make_ntfp_from_tf should be called just prior to pickling
        otherwise the values from the tf graph won't be extracted.
        """
        return (self.ntfp, self.kernel_eval)

    
    def __setstate__(self, d):
        self.ntfp = d[0]
        self.kernel_eval = d[1]
        self.make_tf_from_ntfp()


    def fit(self, problem, data, num_epochs=100, batch_size=50, 
            epochs_per_eval=10, early_stopping_epochs=100):
        """
        The function that learns the model.
        problem is a string
        data must be a DataSet object

        The best model is saved to disk if it is best at the
        evaluation point (every epochs_per_eval)
        """

        # need to save this so we can transform correctly when predicting
        # or when loading in a saved model
        self.ntfp.ztransform = data.ztransform
        
        init = tf.global_variables_initializer()

        with tf.Session() as sess:
            print 'initializing ...'
            sess.run(init)
            print 'evaluating initial accuracy ...'
            best_validation_acc = self.evaluate_validation(sess, data, batch_size=batch_size)
            train_acc = self.evaluate_train(sess, data, batch_size=batch_size)
            best_test_acc = self.evaluate_test(sess, data, batch_size=batch_size)
            best_epoch = -1
            print 'initial acc = ', best_validation_acc, train_acc
            self.make_ntfp_from_tf(sess)
            pickle.dump(self, open('../drkn_models/first_'+problem+'.pkl', 'w', -1))

            #plot_density_map_2d(sess, self.X, self.y_, self, data, 
                                #problem, 0, batch_size=batch_size)
            if self.resultsdb is not None:
                added = self.resultsdb.add_deepresults_to_db(
                    problem, self.ntfp.clf, self.ntfp.delta, best_epoch,
                    1.0-best_test_acc, 1.0-best_validation_acc, 1.0-train_acc,
                    self.ntfp.drop_prob,
                    self.ntfp.l1_coeff, self.ntfp.l2_coeff)
                print added

            # now do training
            for epoch in xrange(num_epochs):
                print 'training epoch ', epoch, ' out of ', num_epochs

                for d, t in data.train_minibatches(batch_size):
                    sess.run(self.train, feed_dict={self.X: d.T, self.y_: t})
                    # cross = sess.run(self.cross_entropy, feed_dict={self.X: d.T, self.y_: t})
                    # l1 = sess.run(self.l1_term, feed_dict={self.X: d.T, self.y_: t})
                    # l2 = sess.run(self.l2_term, feed_dict={self.X: d.T, self.y_: t})
                    #print 'cross, l1, l2 = ', cross, l1, l2

                # evaluate on test data
                if (epoch+1)%epochs_per_eval==0:
                    print 'testing ...', 
                    acc = self.evaluate_validation(sess, data, batch_size=batch_size)
                    train_acc = self.evaluate_train(sess, data,
                                                    batch_size=batch_size)
                    test_acc = self.evaluate_test(sess, data, batch_size=batch_size)
                    print 'test/valid/train acc = ', test_acc, acc, train_acc
                    if self.resultsdb is not None:
                        added = self.resultsdb.add_deepresults_to_db(
                            problem, self.ntfp.clf, self.ntfp.delta, epoch,
                            1.0-test_acc, 1.0-acc, 1.0-train_acc, self.ntfp.drop_prob,
                            self.ntfp.l1_coeff, self.ntfp.l2_coeff)
                        print added
                    if (acc>best_validation_acc):
                        best_validation_acc = acc
                        best_epoch = epoch
                        self.make_ntfp_from_tf(sess)
                        pickle.dump(self, open('../drkn_models/best_'+problem+'.pkl', 'w', -1))
                        best_test_acc = test_acc
                    if epoch-best_epoch>early_stopping_epochs:
                        break

        return best_test_acc, best_validation_acc, best_epoch


    def predict(self, data, batch_size=50):
        """
        predict the class based on the fitted model.
        """
        self.make_tf_from_ntfp()
        init = tf.global_variables_initializer()

        if not isinstance(data, DataSet):
            data = DataSet(data, ztransform=self.ntfp.ztransform,
                           shuffle=False, test_ratio=1.0)
            
        with tf.Session() as sess:
            print 'initializing ...'
            sess.run(init)
            outputs = np.zeros((data.num_test,))
            index = 0
            for d, t in data.test_minibatches(batch_size):
                batch_outputs = sess.run(self.output, feed_dict={self.X: d.T})
                size = batch_outputs.shape[0]
                outputs[index*batch_size:index*batch_size+size] = batch_outputs[:] 
                index += 1
                
        return outputs


    def evaluate_train(self, sess, data, batch_size=50, max_loops=1e10):
        accuracy = 0.0
        num_done = 0
        for d, t in data.train_minibatches(batch_size):
            accuracy += sess.run(self.accuracy,
                                 feed_dict={self.X: d.T, self.y_: t})
            num_done += 1
            if num_done>max_loops:
                break

        return accuracy/num_done


    def evaluate_test(self, sess, data, batch_size=50):
        accuracy = 0.0
        num_done = 0
        for d, t in data.test_minibatches(batch_size):
            accuracy += sess.run(self.accuracy,
                                 feed_dict={self.X: d.T, self.y_: t})
            num_done += 1

        return accuracy/num_done

    def evaluate_validation(self, sess, data, batch_size=50):
        accuracy = 0.0
        num_done = 0
        for d, t in data.validation_minibatches(batch_size):
            accuracy += sess.run(self.accuracy,
                                 feed_dict={self.X: d.T, self.y_: t})
            num_done += 1

        return accuracy/num_done

