"""
Program: deep_radial_kernel_machine
File: read_uci_data.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import pandas
import numpy as np
import os.path as osp
import os
import cPickle as pickle
import sys
from data_set import DataSet
import traceback

def convert_fernandez_data(data_dir, sep='\t', na_values=None):
    """
    Convert the data provided by Fernandez-Delgado from text files into
    pkl files containing a Data object.
    """

    # read all directory names in given data dir
    # for each data dir:
    #    determine if there is a separate test file
    #    if there is:
    #       read both train and test file
    #       split train into train and validation set 75, 25
    #    if not:
    #       read train file
    #       split train into train, valid, test sets 80, 20, 20
    #    
    dir_entries = os.listdir(data_dir)
    for entry in dir_entries:
        try:
            print 'converting ', entry
            entrypath = osp.join(data_dir, entry)
            if osp.isdir(entrypath): # step into directory
                sub_entries = os.listdir(entrypath)
                Rdat = [x for x in sub_entries if '_R.dat' in x]
                # there should be two entries if there is test data
                if len(Rdat)<1 or len(Rdat)>2:
                    raise RuntimeError('Incorrect number of files in: '+entrypath)
                test_data = None
                test_targets = None
                train_base = '_R.dat'
                if len(Rdat)==2:
                    print Rdat
                    testfile = [x for x in Rdat if '_test_' in x]
                    print testfile
                    testfile = osp.join(entrypath, testfile[0])
                    test_datap = pandas.read_table(testfile,sep=sep,
                                                   skipinitialspace=True)
                    test_data = np.array(test_datap)
                    test_targets = test_data[:,-1]
                    test_data = test_data[:,1:-1]
                    train_base = '_train_R.dat'

                trainfile = [x for x in Rdat if train_base in x]
                trainfile = osp.join(entrypath, trainfile[0])
                traindatap = pandas.read_table(trainfile,sep=sep,
                                               skipinitialspace=True)
                train_data = np.array(traindatap)
                train_targets = train_data[:,-1]
                train_data = train_data[:,1:-1]

                dataset = DataSet(train_data, targets=train_targets,
                                  test_data=test_data,
                                  test_targets=test_targets,
                                  normalise=False)

                pklname = osp.join(entrypath, entry+'_data.pkl')
                pickle.dump(dataset, open(pklname, 'wb'), protocol=-1)

        except Exception as e:
            print e
            print traceback.print_exc()
            
            

if __name__=='__main__':

    convert_fernandez_data(sys.argv[1])
