"""
Program: deep_radial_kernel_machine
File: rotate_utils.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tensorflow as tf
import sklearn.svm as svm
import numpy as np
import math

def rotation_from_a_to_b_2d(a, b):
    try:
        aorig = a
        borig = b
        a = a/math.sqrt(np.dot(a.T, a))
        b = b/math.sqrt(np.dot(b.T, b))
        cost = np.dot(a.T, b)
        sint = np.sqrt(1-cost*cost)
        R = np.array([[cost[0,0], -sint[0,0]], [sint[0,0], cost[0,0]]])
        assert np.max(np.abs(np.dot(R, a)-b))<1e-3
        
    except:
        import pdb; pdb.set_trace()
        print 'error'

    return R

def rotation_from_a_to_b(a, b):
    """
    a and b are column vectors
    """

    try:
        a = a/math.sqrt(np.dot(a.T, a))
        I = np.eye(np.max(a.shape), dtype=np.float32)
        c = (a+b)/2.0
        c = c/math.sqrt(np.dot(c.T,c))
        R1 = I - 2*np.dot(a,a.T)
        R2 = I - 2*np.dot(c,c.T)
        R = np.dot(R2,R1)

        assert np.max(np.abs(np.dot(R, a)-b))<1e-3
    except:
        import pdb; pdb.set_trace()
        print 'error'

    return R

def rotation_matrix_two_vecs(u, v, theta):
    """
    create a rotation matrix in N-dimensions that will rotate in the
    plane specified by u and v by the angle theta
    u and v are orthonormal.

    This is a general rotation matrix, but construction is very
    inefficient for axis-aligned vectors.

    equation pilfered from: 
    http://math.stackexchange.com/questions/197772/generalized-rotation-matrix-in-n-dimensional-space-around-n-2-unit-vector
    """
    # make sure vectors are row matrices
    u = u.reshape((np.prod(u.shape),1))
    u = u/np.dot(u.T,u)
    v = v.reshape((np.prod(v.shape),1))
    v = v/np.dot(v.T,v)
    R = np.eye(u.shape[0], u.shape[0], dtype=np.float32)

    uvt = np.dot(u, v.T)
    R += math.sin(theta)*(uvt.T - uvt)
    R += (math.cos(theta)-1)*(np.dot(u,u.T)+np.dot(v,v.T))

    return R


