"""
Program: deep_radial_kernel_machine
File: sqlite_helper.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sqlite3 as lite
import datetime
import os.path

def get_git_hash():
    import sh
    out = sh.git.log(pretty="format:'%h'", n=1).__str__()
    start = out.find("'")
    out = out[start+1:]
    end = out.find("'")
    return int(out[:end], 16)

class SQL_Helper(object):

    def __init__(self, dbase_name='results.db'):
        # this will be fixed once for the current run
        self.datetime = str(datetime.datetime.now())
        self.githash = get_git_hash()
        
        self.dbase_name = dbase_name
        if not os.path.isfile(dbase_name):
            self.sqlite_create(dbase_name)

    def sqlite_create(self, dbase_name):
        with lite.connect(self.dbase_name) as con:
            cur = con.cursor()    
            # two tables - one for svm, one for deep network
            cur.execute("""
                        CREATE TABLE SVMResult(
                        Problem_name VARCHAR(50), 
                        Time VARCHAR(50),
                        Code_version INT,
                        gamma FLOAT,
                        C FLOAT,
                        Error FLOAT)
            """)
            cur.execute("""
                        CREATE TABLE DeepResult(
                        Problem_name VARCHAR(50), 
                        Time VARCHAR(50),
                        Code_version INT,
                        gamma FLOAT,
                        delta FLOAT,
                        TestError FLOAT,
                        ValidError FLOAT,
                        TrainError FLOAT,
                        Epoch INT,
                        drop_prob float,
                        l1_coeff float,
                        l2_coeff float)
            """)
            cur.execute("""
                        CREATE TABLE RBFResult(
                        Problem_name VARCHAR(50), 
                        Time VARCHAR(50),
                        Code_version INT,
                        gamma FLOAT,
                        TestError FLOAT,
                        ValidError FLOAT,
                        TrainError FLOAT,
                        Epoch INT,
                        drop_prob float,
                        l1_coeff float,
                        l2_coeff float)
            """)

    def add_svmresults_to_db(self, problem, svm, error):
        params = svm.estimators_[0].get_params()
        C = params['C']
        gamma = params['gamma']
        # now insert row into table
        fstring = "('%s', '%s', %d, %f, %f, %f)" % (problem, self.datetime,
                                                self.githash, gamma, C, error)
        with lite.connect(self.dbase_name) as con:
            cur = con.cursor()
            cur.execute("""INSERT INTO SVMResult VALUES"""+fstring)
        return fstring
        
    def add_deepresults_to_db(self, problem, svm, delta, epoch,
                              test_error, valid_error, train_error,
                              drop_prob=0.0,
                              l1_coeff=0.005, l2_coeff=0.001):
        params = svm.estimators_[0].get_params()
        gamma = params['gamma']
        # now insert row into table
        fstring = "('%s', '%s', %d, %f, %f, %f, %f, %f, %d, %f, %f, %f)" % (
            problem, self.datetime, self.githash, gamma, delta, test_error,
            valid_error, train_error, epoch, drop_prob, l1_coeff, l2_coeff)

        with lite.connect(self.dbase_name) as con:
            cur = con.cursor()
            cur.execute("""INSERT INTO DeepResult VALUES"""+fstring)
        return fstring

    def add_rbfresults_to_db(self, problem, svm, epoch,
                             test_error, valid_error, train_error,
                             drop_prob=0.0,
                             l1_coeff=0.005, l2_coeff=0.001):
        params = svm.estimators_[0].get_params()
        gamma = params['gamma']
        # now insert row into table
        fstring = "('%s', '%s', %d, %f, %f, %f, %f, %d, %f, %f, %f)" % (
            problem, self.datetime, self.githash, gamma, test_error,
            valid_error, train_error, epoch, drop_prob, l1_coeff, l2_coeff)
        with lite.connect(self.dbase_name) as con:
            cur = con.cursor()
            cur.execute("""INSERT INTO RBFResult VALUES"""+fstring)
        return fstring

    def add_problem_size(self, problem, num_train):
        with lite.connect(self.dbase_name) as con:
            cur = con.cursor()
            fstring = "('%s', %d)" % (problem, num_train)
            exstring = """INSERT INTO ProblemSize VALUES"""+fstring
            print exstring
            cur.execute(exstring)
