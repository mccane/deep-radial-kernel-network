"""
Program: deep_radial_kernel_machine
File: deep_kernel.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tensorflow as tf
import numpy as np
import numpy.random as rand
import math
from rotate_utils import *
from sqlite_helper import SQL_Helper
from compact_kernel import CompactKernel
import itertools
from random_utils import random_uniform_in_sphere
from num_parameters import total_parameters

class DKNonTensorFlowParams(object):
    """
    This class stores all the parameters of the network in numpy arrays
    etc for pickling/unpickling.
    It doesn't actually contain anything explicitly and is constructed
    on the fly in the DeepKernel class.
    """

    def __init__(self):
        self.R = dict()
        self.Rinv = dict()



class DeepKernel(object):
    """
    This class is a RELU approximation to a Gaussian Kernel at least
    initially. After training, it may no longer be a kernel and may be
    quite different from Gaussian.
    """

    ex = np.zeros((2,1), dtype=np.float32)
    ex[0,0] = 1.0

    rplane = np.zeros((2,1), dtype=np.float32)
    rplane[1,0] = -1.0

    def relu_reflect(self, invec, rvec, folds, dim, iterval):
        """
        build a relu reflect layer.
        The relu reflect layer consists of two layers:
        - the first layer rotates to x axis and reflects half of space about x
        - the second layer rotates back and sums result
        - see ICPR paper details on the x-reflection
        """

        Rmat = rotation_from_a_to_b_2d(rvec[0:2,0:1], DeepKernel.ex)
        self.ntfp.R[(folds,dim,iterval)] = Rmat
        self.ntfp.Rinv[(folds,dim,iterval)] = Rmat.T

        return self.relu_reflect_tf_from_ntfp(invec, folds, dim, iterval)

    def relu_reflect_tf_from_ntfp(self, invec, folds, dim, iterval):
        self.R[(folds,dim,iterval)] = tf.Variable(
            self.ntfp.R[(folds,dim,iterval)], name="Rotation")
        invec = tf.matmul(self.R[(folds,dim,iterval)], invec)
        
        # now double the dimensionality of invec and create a
        # positive and negative channel
        double_invec = tf.concat(axis=0, values=[-invec, invec])
        intermediate = tf.nn.relu(double_invec)

        # add the two halves of the intermediate result
        top_one = tf.slice(intermediate, [0,0], [1, -1])
        second = tf.slice(intermediate, [1,0], [1,-1])
        top_half = tf.concat(axis=0, values=[top_one, -second])
        bottom_half = tf.slice(intermediate, [2,0], [-1,-1])
        new_vec = top_half+bottom_half
        
        # now rotate back
        self.Rinv[(folds,dim,iterval)] = tf.Variable(
            self.ntfp.Rinv[(folds,dim,iterval)], name="RotationInv")
        output = tf.matmul(self.Rinv[(folds,dim,iterval)], new_vec)

        return output

    def relu_reflect_ntfp_from_tf(self, sess, folds, dim, iterval):
        self.ntfp.R[(folds,dim,iterval)] = sess.run(self.R[(folds,dim,iterval)])
        self.ntfp.Rinv[(folds,dim,iterval)] = sess.run(
            self.Rinv[(folds,dim,iterval)])

    def fold_1d(self, invec, rvec, rplane, alpha, num_iters,
                folds, dim):
        """
        invec is a tf place holder for the input
        rvec is the initial normal vector specifying the reflection subspace
        rplane is a vector that along with rvec defines the plane in which
            to rotate rvec
        alpha is the initial angle to rotate rvec by. This decreases
            by half each iteration
        num_iters is the number of iterations to apply the folding

        Return value:
        An operation that transforms the input via 
        repeated reflections, projected into the subspace perpendicular
        to the original rvec
        """

        orig_invec = invec
        for iterval in xrange(num_iters):
            invec = self.relu_reflect(invec, rvec, folds, dim, iterval)

            R = rotation_matrix_two_vecs(DeepKernel.ex, rplane, alpha)
            rvec = np.dot(R,rvec)
            alpha /= 2.0

        # no longer need the first dimension
        invec = - tf.slice(invec, [1,0], [-1,-1])
        return invec

    def fold_1d_tf_from_ntfp(self, invec, num_iters, folds, dim):
        for iterval in xrange(num_iters):
            invec = self.relu_reflect_tf_from_ntfp(invec, folds, dim, iterval)

        invec = - tf.slice(invec, [1,0], [-1,-1])
        return invec

    def fold_1d_ntfp_from_tf(self, sess, num_iters, folds, dim):
        for iterval in xrange(num_iters):
            self.relu_reflect_ntfp_from_tf(sess, folds, dim, iterval)
        

    def fold_nd(self, invec, nfolds):
        ndims = invec.get_shape().as_list()[0]
        orig_dims = ndims
        ncentres = invec.get_shape().as_list()[1]

        invec = tf.reshape(invec, [ndims, -1])
        rvec = DeepKernel.ex
        print 'folding dims', ndims
        nfold_networks = int(math.ceil(math.log(ndims,2)))
        self.invecs = []
        for folds in xrange(nfold_networks):
            foldvecs = []
            print 'fold num', folds
            for dim in xrange(ndims/2):
                print 'folding dims', dim*2, dim*2+1
                thisvec = tf.slice(invec, [dim*2,0], [2,-1])
                foldvecs += [self.fold_1d(thisvec, rvec, DeepKernel.rplane,
                                          math.pi/2.0, nfolds, folds, dim)]
            if ndims%2==1:
                thisvec = tf.slice(invec, [ndims-1,0], [1,-1])
                foldvecs += [thisvec]
            invec = tf.concat(axis=0, values=foldvecs)
            self.invecs += [invec]
            ndims = len(foldvecs)

        self.total_layers = nfold_networks*nfolds
        self.nfolds = nfolds
        self.fold_neurons = orig_dims*nfolds*4
        print 'total layers = ', nfold_networks*nfolds
        invec = tf.reshape(invec, [ncentres, -1])
        return invec

    def fold_nd_tf_from_ntfp(self, invec, nfolds):
        ndims = invec.get_shape().as_list()[0]
        ncentres = invec.get_shape().as_list()[1]

        invec = tf.reshape(invec, [ndims, -1])
        print 'folding dims', ndims
        nfold_networks = int(math.ceil(math.log(ndims,2)))
        for folds in xrange(nfold_networks):
            foldvecs = []
            print 'fold num', folds
            for dim in xrange(ndims/2):
                print 'folding dims', dim*2, dim*2+1
                thisvec = tf.slice(invec, [dim*2,0], [2,-1])
                foldvecs += [self.fold_1d_tf_from_ntfp(thisvec, nfolds,
                                                       folds, dim)]
            if ndims%2==1:
                thisvec = tf.slice(invec, [ndims-1,0], [1,-1])
                foldvecs += [thisvec]
            invec = tf.concat(axis=0, values=foldvecs)
            ndims = len(foldvecs)

        self.total_layers = nfold_networks*nfolds
        print 'total layers = ', nfold_networks*nfolds
        invec = tf.reshape(invec, [ncentres, -1])
        return invec

    def fold_nd_ntfp_from_tf(self, sess, invec, nfolds):
        ndims = invec.get_shape().as_list()[0]
        ncentres = invec.get_shape().as_list()[1]

        invec = tf.reshape(invec, [ndims, -1])
        print 'folding dims', ndims
        nfold_networks = int(math.ceil(math.log(ndims,2)))
        for folds in xrange(nfold_networks):
            for dim in xrange(ndims/2):
                self.fold_1d_ntfp_from_tf(sess, nfolds, folds, dim)
            ndims = ndims/2 + ndims%2
        
    def __getstate__(self):
        """
        Should call fold_nd_ntfp_from_tf before pickling to ensure
        the relevant values are copied out of the tf graph
        """
        return self.ntfp

    def __setstate__(self, d):
        """
        Need to call fold_nd_tf_from_ntfp after unpickling
        with an appropriate invec
        """
        self.ntfp = d
            

    def find_linear_approx_to_kernel(self, sigma, delta):
        self.ck = CompactKernel(sigma)
        self.ck.approximate_to_delta(delta)

        theoretical = 3*3.76*sigma*self.ck.max_abs_d()/delta

    def eval_actual_kernel(self, diffs):
        diffs2 = tf.multiply(diffs, diffs)
        distance2 = tf.reduce_sum(diffs2, 0)
        exp = tf.exp(-self.ntfp.gamma*distance2)
        return exp

    def eval_approx_kernel(self, diffs):
        diffs2 = tf.multiply(diffs, diffs)
        r = tf.sqrt(tf.reduce_sum(diffs2, 0))
        e = (1-r/(3.76*self.ntfp.sigma))**4 * (4*r/(3.76*self.ntfp.sigma) + 1)
        return e
        

    def make_tf_from_ntfp(self, support_vectors, X):
        self.find_linear_approx_to_kernel(self.ntfp.sigma, self.ntfp.delta)
        
        self.R = dict()
        self.Rinv = dict()
        # experiment two - dropout entire support vector
        self.drop_out_centres = tf.nn.dropout(
            support_vectors, 1.0-self.ntfp.drop_prob,
            noise_shape=[self.ntfp.num_dims, 1])
        self.expanded_centres = tf.expand_dims(self.drop_out_centres, 2)
        expanded_inputs = tf.expand_dims(X, 1)
        self.diffs = tf.subtract(expanded_inputs, self.expanded_centres)
        self.fold = self.fold_nd_tf_from_ntfp(self.diffs, self.ntfp.num_folds)
        self.kernel = self.ck.make_tf_graph(self.fold, self.ntfp.delta)
        self.actual_kernel = self.eval_actual_kernel(self.diffs)


    def make_ntfp_from_tf(self, sess):
        self.fold_nd_ntfp_from_tf(sess, self.diffs, self.ntfp.num_folds)
        
    def total_neurons(self):
        return self.fold_neurons+self.ck.total_neurons()
    
    def __init__(self, support_vectors, gamma, delta, X, drop_prob=0.5):
        """
        clf is an instance of a sklearn.OneVersusRestClassifier with
        an svm as the underlying classifier with a Gaussian kernel.
        delta is the preferred approximation level of the kernel function.
        X is the placeholder input

        From Lemma 19 of Shamir et al. and my work
        Given an svm with a particular sigma, and a required delta:
        First, fold up space, to error <= delta for a given sigma
        Then:
        L = CompactKernel.max_abs_d()
        R = 3.76 sigma
        w = ceil(3*R*L/delta) <- number of relus in final layer
        set a, betai, alphai according to lemma (there are w of them)

        OK, this method is nuts because the number of units is 
        inversely proportional to the required error. So an error of 0.001
        requires more than 6000 units! Try something more practical.

        Managed to reduce the number of units down to 20-25% of the uniform
        intervals.
        """

        self.ntfp = DKNonTensorFlowParams()
        self.ntfp.delta = delta
        self.ntfp.drop_prob = drop_prob

        # this creates the appropriate intervals etc
        # libsvm apparently uses gamma = 1/(2 sigma^2)
        self.ntfp.gamma = gamma
        gamma = 1.0/math.sqrt(2.0*gamma)
        self.ntfp.sigma = gamma
        R = 3.76*gamma

        # for each support vector, add a new kernel set by calling fold_nd
        # with different centres (but same input)

        self.ntfp.num_dims = int(support_vectors.get_shape()[0])
        num_dims = self.ntfp.num_dims
        num_folds = num_dims/np.log2(num_dims) * np.log2(R*math.pi/math.sqrt(delta))

        # self.ntfp.num_folds = min(int(math.ceil(num_folds)), 10)
        self.ntfp.num_folds = int(math.ceil(num_folds))
        print 'num_folds = ', self.ntfp.num_folds
        print 'num sv = ', support_vectors.get_shape()[1]
        
        self.find_linear_approx_to_kernel(self.ntfp.sigma, self.ntfp.delta)
        self.R = dict()
        self.Rinv = dict()
        
        # experiment two - dropout entire support vector
        self.drop_out_centres = tf.nn.dropout(
            support_vectors, 1.0-self.ntfp.drop_prob,
            noise_shape=[self.ntfp.num_dims, 1])
        self.expanded_centres = tf.expand_dims(self.drop_out_centres, 2)
        expanded_inputs = tf.expand_dims(X, 1)
        self.diffs = tf.subtract(expanded_inputs, self.expanded_centres)
        self.fold = self.fold_nd(self.diffs, self.ntfp.num_folds)
        self.kernel = self.ck.make_tf_graph(self.fold, self.ntfp.delta)
        self.actual_kernel = self.eval_actual_kernel(self.diffs)
        self.approx_kernel = self.eval_approx_kernel(self.diffs)


if __name__=='__main__':
    """
    Do some testing on deep_kernel
    """

    import argparse

    parser = argparse.ArgumentParser(description='Test fn approx.')
    parser.add_argument('--num_dims', type=int, nargs='+', default=[10],
                        help='Number of dimensions of problem')
    parser.add_argument('--batch_size', type=int,
                        default=50,
                        help='batch size')
    parser.add_argument('--num_batches', type=int,
                        default=20,
                        help='Number of batches to test')
    parser.add_argument('--sigma', type=float, nargs='+', default=[1.0],
                        help='standard deviation of Gaussian')
    parser.add_argument('--delta', type=float, nargs='+', default=[0.1],
                        help='required accuracy (maximum error)')
    parser.add_argument('--outfile', type=str, default='deep_kernel_results.dat',
                        help='file to put results in')

    args = parser.parse_args()

    # ndimss = [2**i for i in xrange(1, int(math.log(args.num_dims)/math.log(2))+1)]
    ndimss = args.num_dims
    nbatches = args.num_batches
    batch_size = args.batch_size
    sigmas = args.sigma
    deltas = args.delta
    
    with open(args.outfile, 'a') as outfile:
        outfile.write('NumDims, Delta, Sigma, maxNormError, maxError, numNeurons, totalParameters\n')
    fstring = '{:d}, {:f}, {:f}, {:.2e}, {:.2e}, {:d}, {:d}\n'
        
    for ndims,sigma,delta in itertools.product(ndimss,sigmas,deltas):
        gamma = 1.0/(2*sigma*sigma)
        svs = tf.Variable(np.zeros((ndims, 1)), dtype=np.float32)
        X = tf.placeholder(tf.float32, (ndims, None))
        deep_kernel = DeepKernel(svs, gamma, delta, X, 0.0)
        ck = deep_kernel.ck
    
        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            max_norm_error = 0.0
            max_func_error = 0.0
            sess.run(init)

            for batch in xrange(nbatches):
                data = random_uniform_in_sphere(3.0*sigma,ndims, batch_size)

                fold_nd = sess.run(deep_kernel.fold, feed_dict={X: data})
                deep_eval = sess.run(deep_kernel.kernel, feed_dict={X: data})
                approx = sess.run(deep_kernel.approx_kernel, feed_dict={X: data})

                norms = np.array([np.dot(data[:,i:i+1].T,data[:,i:i+1])[0,0] for i in xrange(data.shape[1])])
                norms = np.sqrt(norms)

                norm_evals = np.array([ck.eval(norms[i]) for i in xrange(norms.shape[0])])
                norm_evals = np.reshape(norm_evals, (1, norms.shape[0]))
            
                fold_evals = np.array([ck.eval(fold_nd[0,i]) for i in xrange(fold_nd.shape[1])])
                fold_evals = np.reshape(fold_evals, (1, fold_nd.shape[1]))
                batch_max_norm_error = np.max(np.abs(fold_nd-norms))
                batch_max_func_error = np.max(np.abs(deep_eval-approx))
                if batch_max_norm_error>max_norm_error:
                    max_norm_error = batch_max_norm_error
                if batch_max_func_error>max_func_error:
                    max_func_error = batch_max_func_error

            print 'deep = ', fstring.format(ndims, delta, sigma,
                                            max_norm_error,
                                            max_func_error,
                                            deep_kernel.total_neurons(),
                                            total_parameters())
            with open(args.outfile, 'a') as outfile:
                outfile.write(fstring.format(ndims, delta, sigma,
                                             max_norm_error,
                                             max_func_error,
                                             deep_kernel.total_neurons(),
                                             total_parameters()))
