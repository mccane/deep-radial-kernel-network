import sklearn.base as skb
import sklearn.svm as svm
import random
from sklearn import preprocessing
import numpy.random as npr
import numpy as np
from read_uci_data import read_uci_data
from data_set import DataSet

class BaseRandomSVC(skb.BaseEstimator, skb.ClassifierMixin):
    """
    A random support vector classifier.

    In this context, random means that the support vectors are
    chosen randomly. This is not expected to work very well and is only
    useful as an initialliser for a DRKN when we want to specify the 
    number of support vectors exactly. And this is useful if the usual
    SVC chooses too many support vectors for the current implementation of
    DRKN to handle.

    Limitation: this is a strictly two-class SVM. The target class has
    label 1, all other labels are non-target. This should fit in with
    OneVersusRestClassifier
    """

    def __init__(self, num_support_vectors=1000, gamma=0.1, C=1.0):
        # parameter C is ignored
        self.num_support_vectors_=num_support_vectors
        self._gamma=gamma
        self.C_=C


    def get_params(self, deep=False):
        return {'num_support_vectors': self.num_support_vectors_,
                'gamma': self._gamma,
                'C': self.C_}

    def fit(self, X, y):
        num_data, num_features = X.shape
                  
        random.seed()
        # need to make sure I have an equal number from
        # positive and negative examples.
        # use np.where to get indices of positive examples
        pos_indices = np.where(y==1)[0]
        neg_indices = np.where(y==0)[0]
        num_support = min(len(pos_indices), len(neg_indices), self.num_support_vectors_)

        pos_support = random.sample(pos_indices, num_support)
        neg_support = random.sample(neg_indices, num_support)
        self.support_ = np.concatenate((pos_support, neg_support))
        self.num_support_vectors_ = len(self.support_)
        self.support_vectors_ = X[self.support_,:]

        coefs = np.ones((1, X.shape[0]))
        # class 1 is the target class
        coefs[0,y!=1] = -1.0

        # actually, we want random coefficients to start
        # multiply by normal random var
        rans = npr.rand(1, X.shape[0])
        coefs = np.multiply(coefs, rans)
        
        self.dual_coef_ = coefs[0:1,self.support_]
        
        self.intercept_ = 0.0
        self.n_support_ = [self.num_support_vectors_]
        
    def decision_function(self, X):
        diff = [X-self.support_vectors_[i:i+1,:] for i in xrange(self.support_vectors_.shape[0])]
        nsv = self.n_support_[0]
        dist = np.zeros((X.shape[0], nsv))
        for sv in xrange(nsv):
            for vec in xrange(X.shape[0]):
                dist[vec, sv] = np.sum(np.multiply(diff[sv][:,vec:vec+1], diff[sv][:,vec:vec+1]))
        val = np.exp(-self._gamma*dist)
        val = np.dot(val,self.dual_coef_.T)+ self.intercept_
        return val

    def predict(self, X):
        y = self.decision_function(X)
        y[y>0] = 1
        y[y<=0] = 0
        return y

class RandomSVC(skb.BaseEstimator, skb.ClassifierMixin):

    def __init__(self, num_support_vectors=1000, gamma=0.1, C=1.0,
                 base_svcs=[]):
        # parameter C is ignored
        self.num_support_vectors_=num_support_vectors
        self._gamma=gamma
        self.C_=C
        self.base_svcs_ = base_svcs

    def fit(self, X, y):
        self.lb = preprocessing.LabelBinarizer()
        self.lb.fit(y)
        y = self.lb.transform(y)
        # stupid LabelBinarizer does a different transformation in the case
        # of just two classes
        if y.shape[1]==1:
            y[:,0] = 1-y[:,0]
            self.inverse_ = True

        nsvms = y.shape[1]
        for i in xrange(nsvms):
            self.base_svcs_ += [BaseRandomSVC(self.num_support_vectors_,
                                              self._gamma,
                                              self.C_)]
            self.base_svcs_[i].fit(X, y[:,i])

    def decision_function(self, X):
        nsvms = len(self.base_svcs_)
        decision = np.ndarray((X.shape[0], nsvms))
        for i in xrange(nsvms):
            decision[:,i:i+1] = self.base_svcs_[i].decision_function(X)

        return decision

    def predict(self, X):
        nsvms = len(self.base_svcs_)
        predict = np.ndarray((X.shape[0], nsvms))
        for i in xrange(nsvms):
            predict[:,i:i+1] = self.base_svcs_[i].predict(X)
            
        predict = self.lb.inverse_transform(predict)
        return predict

    def get_params(self, deep=False):
        return {'num_support_vectors': self.num_support_vectors_,
                'gamma': self._gamma,
                'C': self.C_,
                'base_svcs': self.base_svcs_}


if __name__=='__main__':
    data, targets = read_uci_data('winequality-white.csv',sep=';')
    data = DataSet(data, targets)

    clf = RandomSVC(500)
    # clf = svm.SVC(3)
    clf.fit(data.train_data, data.train_targets) 

    res = clf.predict(data.test_data)

    print 'computing error'

    diff = np.abs(res-data.test_targets)
    error = len(diff[diff!=0])/float(len(res))

    print 'error =', error



        
