from tensorflow.examples.tutorials.mnist import input_data
from sklearn import svm
import cPickle as pickle
from sklearn.multiclass import OneVsRestClassifier
import sklearn.datasets as data
import numpy as np
import numpy.random as rand
from data_set import DataSet
from read_uci_data import read_uci_data
import sys
import scipy.stats
import sklearn.grid_search as skg
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--problem", dest='problem',
                    help="the problem name", type=str)
parser.add_argument("--iters", dest='iters',
                    help='the number of random parameter choices',
                    type=int, default=20)
parser.add_argument('--njobs', dest='njobs', type=int, default=3,
                    help='the number of concurrent processes to run')
parser.add_argument('--gamma_scale', dest='gscale', type=float, default=0.1,
                    help='the scale of exponential distribution of gamma')
parser.add_argument('--C_scale', dest='cscale', type=float, default=100,
                    help='the scale of exponential distribution of C')
args = parser.parse_args()

problem = args.problem
niters = args.iters
njobs = args.njobs
gscale = args.gscale
cscale = args.cscale

print 'training'
clf = OneVsRestClassifier(svm.SVC())
params = {'estimator__C': scipy.stats.expon(scale=cscale), 'estimator__gamma': scipy.stats.expon(scale=gscale)}

random_search = skg.RandomizedSearchCV(clf, params, n_iter=niters, n_jobs=njobs, verbose=5)

if problem=='covtype':
    # covtype = data.fetch_covtype(shuffle=True)
    # data = DataSet(covtype['data'][:100000,:], covtype['target'][:100000], test_ratio=0.1)
    # pickle.dump(data, open('covtype_data.pkl', 'wb'), protocol=-1)
    data = pickle.load(open('../data/covtype_data.pkl', 'rb'))
    # clf = OneVsRestClassifier(svm.SVC(C=56.47, gamma=0.49))
    

elif problem=='shuttle':
    train_data, train_targets = read_uci_data('shuttle.trn')
    test_data, test_targets = read_uci_data('shuttle.tst')

    data = DataSet(train_data, train_targets, test_data, test_targets)


elif problem=='redwine':
    data, targets = read_uci_data('winequality-red.csv',sep=',')
    data = DataSet(data, targets)

elif problem=='whitewine':
    data, targets = read_uci_data('winequality-white.csv',sep=';')
    data = DataSet(data, targets)

elif problem=='svmguide1':

    train_data, train_targets = read_uci_data('svmguide1_train.data')
    test_data, test_targets = read_uci_data('svmguide1_test.data')

    data = DataSet(train_data, train_targets, test_data, test_targets)


elif problem=='svmguide3':

    train_data, train_targets = read_uci_data('svmguide3_train.data')
    # test_data, test_targets = read_uci_data('svmguide3_test.data')

    data = DataSet(train_data, train_targets) #, test_data, test_targets)

elif problem=='breastcancer':
    train_data, train_targets = read_uci_data('breast-cancer-wisconsin.data', sep=',', ignore_cols=[0], na_values='?')
    train_targets[train_targets==2] = 0
    train_targets[train_targets==4] = 1
    data = DataSet(train_data, train_targets) #, test_data, test_targets)

elif problem=='cifar10':
    data = pickle.load(open('../Data/cifar-10-batches-py/cifar10_dataset.pkl', 'rb'))

elif problem=='cifar10_d1':
    data = pickle.load(open('../Data/cifar-10-batches-py/cifar10_dataset_d1.pkl', 'rb'))

elif problem=='sat':
    train_data, train_targets = read_uci_data('sat.trn', sep=' ')
    test_data, test_targets = read_uci_data('sat.tst', sep=' ')
    data = DataSet(train_data, train_targets, test_data, test_targets)

elif problem=='sensorlessdrive':
    train_data, train_targets = read_uci_data('Sensorless_drive_diagnosis.txt', sep=' ')
    data = DataSet(train_data, train_targets, test_ratio=0.8)

elif problem=='segment':
    train_data, train_targets = read_uci_data('segment.dat', sep=' ', ignore_cols=[2])
    data = DataSet(train_data, train_targets)

elif problem=='spiral':
    train_data, train_targets = pickle.load(open('../Data/spiral_dat.pkl', 'rb'))
    data = DataSet(train_data, train_targets)

#if problem != 'covtype':
random_search.fit(data.train_data, data.train_targets)

clf = random_search.best_estimator_

print 'best_params = ', random_search.best_params_, 'best_score = ', random_search.best_score_
print 'testing'
res = clf.predict(data.test_data)

print 'computing error'

diff = np.abs(res-data.test_targets)
error = len(diff[diff!=0])/float(len(res))

print 'error =', error

print 'dumping'
pickle.dump(clf, open("svm_"+problem+"_ovr.skl",'w'), protocol=-1) 
