"""
Program: deep_radial_kernel_machine
File: plot_results.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sqlite3 as lite
import numpy as np
import matplotlib.pyplot as plt

def get_best_results_from_db(dbase_name):
    with lite.connect(dbase_name) as con:
        cur = con.cursor()
        cur.execute("""
                    select problem_name, testerror, validerror, 
                    trainerror, l2_coeff, epoch, num_train from 
                    deepresult join problemsize using (problem_name)
                    order by num_train, problem_name, validerror, epoch;
        """)
        deep_results_list = cur.fetchall()
        deep_results = dict()
        name_order = []
        for problem_name, test_error, valid_error, train_error, l2_coeff, epoch, num_train in deep_results_list:
            if num_train>0 and deep_results.get(problem_name) is None:
                deep_results[problem_name] = (test_error, valid_error, train_error, l2_coeff, epoch, num_train)
                name_order += [problem_name]

        cur.execute("""
                    select problem_name, testerror, validerror, 
                    trainerror, l2_coeff, epoch, num_train from 
                    rbfresult join problemsize using (problem_name)
                    order by num_train, problem_name, validerror, epoch;
        """)
        rbf_results_list = cur.fetchall()
        rbf_results = dict()
        for problem_name, test_error, valid_error, train_error, l2_coeff, epoch, num_train in rbf_results_list:
            if rbf_results.get(problem_name) is None:
                rbf_results[problem_name] = (test_error, valid_error, train_error, l2_coeff, epoch, num_train)
        
        cur.execute("""
                    select problem_name, error from svmresult;
        """)
        svm_results_list = cur.fetchall()
        svm_results = dict()
        for problem_name, error in svm_results_list:
            if svm_results.get(problem_name) is None:
                svm_results[problem_name] = error

        
        deep_keys = set(deep_results.keys())
        svm_keys = set(svm_results.keys())
        common_keys = deep_keys & svm_keys

        svm_errors = []
        deep_errors = [] 
        for name in name_order:
            if name in common_keys:
                svm_errors += [svm_results[name]]
                deep_errors += [deep_results[name][0]]

        indices = np.argsort(deep_errors)
        svm_errors = np.array(svm_errors)
        deep_errors = np.array(deep_errors)

        ind = np.arange(len(svm_errors))  # the x locations for the groups
        width = 0.35
        fig, ax = plt.subplots()
        rects1 = ax.bar(ind, svm_errors[indices], width, color='r')

        rects2 = ax.bar(ind + width, deep_errors[indices], width, color='y')

        # add some text for labels, title and axes ticks
        ax.set_ylabel('Error')

        ax.legend((rects1[0], rects2[0]), ('SVM', 'Deep Network'))

        ax.set_title("Results on many problems from UCI dataset")
        plt.tight_layout()
        plt.savefig("results.pdf")


if __name__=='__main__':
    get_best_results_from_db("../drkn_models/results.db")

    
