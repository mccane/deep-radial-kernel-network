"""
Program: deep_radial_kernel_machine
File: uber_approx.py
Copyright (C) 2017, Brendan McCane (mccane@cs.otago.ac.nz)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from subprocess import call, check_call
import itertools
import argparse

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Test fn approx.')
    parser.add_argument('--approximator', type=str, nargs='+',
                        default=['deep_kernel.py'])
    parser.add_argument('--num_dims', type=int, nargs='+', default=[10],
                        help='Number of dimensions of problem')
    parser.add_argument('--batch_size', type=int,
                        default=50,
                        help='batch size')
    parser.add_argument('--num_batches', type=int,
                        default=20,
                        help='Number of batches to test')
    parser.add_argument('--sigma', type=float, nargs='+', default=[1.0],
                        help='standard deviation of Gaussian')
    parser.add_argument('--delta', type=float, nargs='+', default=[0.1],
                        help='required accuracy (maximum error)')

    args = parser.parse_args()

    approximators = args.approximator
    
    ndimss = args.num_dims
    nbatches = args.num_batches
    batch_size = args.batch_size
    sigmas = args.sigma
    deltas = args.delta

    for ndims,sigma,delta,approximator in itertools.product(ndimss,sigmas,deltas,approximators):
        outfile = approximator[:-3]+'_results.dat'

        call(['python', approximator,
              '--outfile', outfile,
              '--num_dims',str(ndims),
              '--batch_size',str(batch_size),
              '--num_batches',str(nbatches),
              '--sigma',str(sigma),
              '--delta',str(delta)])
