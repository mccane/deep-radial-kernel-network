import matplotlib.pyplot as plt

def read_file(filename):
    with open(filename) as f:
        lines = f.readlines()
        data = {}
        for line in lines[1:]:
            entries = line.split(",")
            nums = [float(x) for x in entries]
            data[tuple(nums[0:3])] = nums[3:]

    return data

def extract_data(data, k1index, k2index, dindex, k1const, k2const):
    keys = data.keys()
    data_tuple = []
    xset = set([0,1,2])-set([k1index,k2index])
    xindex = list(xset)[0]
    
    for key in keys:
        if key[k1index]==k1const and key[k2index]==k2const:
            data_tuple += [(key[xindex], data[key][dindex])]

    data_tuple.sort()
    dims = [x for x,y in data_tuple]
    neurons = [y for x,y in data_tuple]
    return dims, neurons
                    
if __name__=='__main__':
    deep_data = read_file('deep_kernel_results.dat')
    shallow_data = read_file('three_layer_approx_results.dat')

    deep_dims, deep_neurons = extract_data(deep_data, 1, 2, 2, 0.01, 2.0)
    plt.plot(deep_dims, deep_neurons)
    shallow_dims, shallow_neurons = extract_data(shallow_data, 1, 2, 2, 0.01, 2.0)
    plt.plot(shallow_dims, shallow_neurons)
    plt.show()
    import pdb; pdb.set_trace()
    print 'done'
